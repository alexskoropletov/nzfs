/**
 * Плагин для реализации функционала всплывающих окон
 * @author		Roman Kudlay (Roman.Kudlay@softline.ru)
 * @since		25.02.2011
 * @copyright	Softline, 2011
 *
 * Вызов плагина:
 * $(селектор диалогового окна, элементов может быть несколько).popup(параметры)
 * Количество и порядок параметров может быть любым, главное: тип и порядок однотипных параметров
 *
 * Строка:  Селектор эемента при клике по которому происходит отображение диалогового окна
 * Функция: первый параметр: функция onAction
 *          второй параметр: функция onCancel
 * Объект:  Дополнительные параметры диалогового окна, их может быть много, все параметры будут объеденины в общий массив
 * в остальными параметрами, в том числе селекторами и callback функциями и значениями по умолчанию
 *
 * Параметры:
 * параметр(значение по умолчанию): описание
 *
 * overlayClass (popupOverlay):             Класс для оверлея если используется модальное окно
 * waitingClass (waitingAnimation):         Класс для индикатора ожидания
 * overlayOpacity (0.01):					Прозрачность фона
 * closeClass (popupClose):                 Класс для элементов скрытия
 * actionClass (popupAction):               Класс для действий
 * contentClass (popupContent):				Класс для содержимого
 * hideOnAction (true):                     Скрывать окно при действии
 * width (360):                             Задать ширину окна,
 * modal (true):                            Модальное окно
 * onOpen (false):                          Действие при отображении окна
 * onAction (false):                        Действие при выборе кнопки
 * onCancel (false):                        Действие при отмене
 * onClose (false):                         Действие при закрытии
 * activator (false):                       Элемент активатора
 * activatorObj (undefined):                Объект который вызвал отображение окна
 * liveActivator (true):                    Связывать ли действия с новыми активаторами. Если нет, то вновь созданные
 *          элементы не будут вызывать диалоговое окно
 * key (undefined):                         Ключ для связи с вызовом
 * dataName (popupOptions):                 Ключ для хранения данных
 * ajaxMode (false):                        Режим работы в ajax режиме, может принимать следующие значения
 *          false   - ajax не используется
 *          json    - возвращаемое значение в виде объекта передается в функцию onAjaxLoad
 *          content - возвращаемое значение заменит содержимое contentClass
 * ajaxData (false):                        Данные, передаваемые на сервер в режиме ajax
 * ajaxUrl (false):                         URL коннектора в ajax режиме
 * onAjaxLoad (false)                       Обработчик события успешной загрузки данных в ajax режиме
 *
 * Примеры использования:
 * @Language javascript
 * $('.popupmenu').popup();
 * $('.popupmenu').popup('#activator', function(){ alert('success')}, function(){ alert('cancel'}, {modal: false});
 * $('.popupmenu').popupOpen({onOpen: openFunc});
 * $('.popupmenu').popupOpen({id: 123});
 * $('.popupmenu').popupClose();
 *
 * function openFunc( element, options ) {
 *    $(el).find(options.b-mail_window_content).load('/ajax/content.php?id'+options.key);
 * }
 *
 * $('.popupmenu').popup('.activator', {
 *     onOpen:      openFunc,
 *     onAction:    doAction
 * });
 * function openFunc( element, options ) {
 *    // Получение родительского элемента внутри которого бына нажата кнопка активации диалогового окна
 *    container = $(options.activatorObj).parents('.container');
 *    id контейнера вида message_1234, получаем id записи
 *    var id = container.attr('id').replace('message_','');
 *    // Загрузка в диалог данных
 *    $(element).find(options.contentClass).load('/ajax/content.php?id='+id);
 *    // В options добавляетя запись о id записи
 *    return {key: id};
 * }
 *
 * function doAction( element, index, key, options ) {
 *    // На диалоговом окне 2 кнопки кроме отмены
 *    switch(index) {
 *      case(0):
 *          alert("Вы удаляете запись " + key);
 *      break;
 *      case(1):
 *          // Получения значения поля ввода в диалоговом окне
 *          var val = $(element).find('#input_name').val();
 *          // Проверка на пустое значение
 *          if (val == '') {
 *              // Вывод текста в диалоговое окно
 *              $(element).find('#message').text('Необходимо ввести имя');
 *              // Запрет выхода из диалога
 *              return false
 *          }
 *      break;

 *    }
 * }
 *
 *
 * <a href="#" onclick="$('.popupmenu').popupOpen(<?=ID?>);">Открыть</a>
 * Параметры вызова callback функций
 *
 * function onOpen( element, options )
 * element  - элемент диалогового окна
 * options  - параметры диалогового окна
 *
 * Функция вызывается перед отображение окна, в ней могут производится действия по модификации окна, например, загрузка
 * содержимого при помощи ajax запроса на сервер
 *
 * ---------------------------------------------------------------------------------------------------------------------
 *
 * function onAction( element, index, key, options )
 * element  - элемент диалогового окна
 * index    - номер кнопки действия, необходимо для определения какая именно кнопка была нажата
 * key      - ключ, переменная, которая была передана в функцию popupOpen()
 * options  - параметры диалогового окна
 *
 * Функция вызывается при нажатии на кнопку действия, кнопок может быть несколько, тогда определение какая именно кнопка
 * была нажата производится по значению переменной index
 *
 * ---------------------------------------------------------------------------------------------------------------------
 *
 * function onAjaxLoad( element, data, options )
 * element  - элемент диалогового окна
 * data     - данные, полученные от сервера
 * options  - параметры диалогового окна
 *
 * Функция вызывается когда получен ответ от сервера
 *
 * ---------------------------------------------------------------------------------------------------------------------
 *
 * function onCancel( element, key, options )
 * element  - элемент диалогового окна
 * key      - ключ, переменная, которая была передана в функцию popupOpen()
 * options  - параметры диалогового окна
 *
 * Функция вызывается при нажатии кнопки отмены на окне
 * Функция должна возвратить true в случае успеха, иначе окно не будет закрыто
 *
 * ---------------------------------------------------------------------------------------------------------------------
 *
 * function onClose( element, key, options )
 * element  - элемент диалогового окна
 * key      - ключ, переменная, которая была передана в функцию popupOpen()
 * options  - параметры диалогового окна
 *
 * Функция вызывается при закрытии окна как в случае выбора действия, так и при нажатии отмены
 * Функция должна возвратить true в случае успеха, иначе окно не будет закрыто
 */
;(function($) {
    $.fn.popup = function() {
        // Массив значений по умолчанию
        var defaults = {
            /** Класс для оверлея */
            overlayClass:   'popupOverlay',
            /** Класс для индикатора ожидания */
            waitingClass:   'waitingAnimation',
			/** Прозрачность фона */
			overlayOpacity:	0.01,
            /** Класс для элементов скрытия */
            closeClass:     'popupClose',
            /** Класс для действий */
            actionClass:    'popupAction',
            /** Класс для содержимого */
            contentClass:   'popupContent',
            /** Скрывать окно при действии */
            hideOnAction:   true,
            /** Ширина окна */
            width:          360,
            /** Модальное окно */
            modal:          true,
            /** Действие при отображении окна */
            onOpen:         false,
            /** Действие при выборе кнопки */
            onAction:       false,
            /** Действие при отмене */
            onCancel:       false,
            /** Действие при закрытии */
            onClose:        false,
            /** Элемент активатора */
            activator:      false,
            /** Объект который вызвал отображение окна */
            activatorObj:   undefined,
            /** Связывать ли действия с новыми активаторами */
            liveActivator:  true,
            /** Ключ для связи с вызовом */
            key:            undefined,
            /** Ключ для хранения параметров */
            dataName:       'popupOptions',
            /** Режим работы в ajax режиме */
            ajaxMode:       false,
            /** Данные, передаваемые на сервер в режиме ajax */
            ajaxData:       false,
            /** URL коннектора в ajax режиме */
            ajaxUrl:        false,
            /** Обработчик события успешной загрузки данных в ajax режиме */
            onAjaxLoad:     false
        };

        var options = {};
        // Обработка параметров функции
        for (var iParNum = 0; iParNum < arguments.length; iParNum++) {
            var Param = arguments[iParNum];
            switch (typeof(Param)) {
                // Строка: активатор
                case('string'):
                    if (options.activator == undefined) {
                        options.activator = Param;
                    }
                break;
                // Функция: обработчик событий
                case('function'):
                        if (options.onAction == false){
                            options.onAction = Param;
                        } else if(options.onCancel == false) {
                            options.onCancel = Param;
                        }
                break;
                // Настройки
                case('object'):
                    options = $.extend(options, Param);
                break;
            }
        }
        
        // Совмещение параметров
        options = $.extend(defaults, options);
        // Инициализация диалоговых окон
        return this.each(function(index, el) {
            var $el = $(el);
            $(this).detach().appendTo('body'); // Перемещение окна в body для обхода ошибки в IE 7
            $el.data('popupOptions', options);
            // Навешивание обработчика закрытия окна
            $el.find('.'+options.closeClass).click(function(e){
                e.preventDefault();
                $el.popupClose();
            });
            // Навешивание обработчика действия
            $el.find('.'+options.actionClass).each(function(i){
                $(this).click(function(e){
                    e.preventDefault();
                    $el.popupAction(i);
                });
            });
            // Навешивание действия отображения окна на селектор
            if (options.liveActivator) {
                if (options.activator) {
                    $(options.activator).live('click', function(e){
                        e.preventDefault();
                        $el.popupOpen(undefined, this);
                    });
                }
            } else {
                if (options.activator) {
                    $(options.activator).click(function(e){
                            e.preventDefault();
                            $el.popupOpen(undefined, this);
                        });
                }
            }
        });
    }
    /**
     * Функция открытия диалогового окна
     * @param key дополнительная информация
     */
    $.fn.popupOpen = function(key, activator, options) {
        return this.each(function() {
            $.popup.open(this, key, activator, options)
        });
    };
    /**
     * Функция закрытия диалогового окна
     */
    $.fn.popupClose = function() {
        return this.each(function() {
            $.popup.close(this, false)
        });
    };

    /**
     * Функция выполения действия
     */
    $.fn.popupAction = function(index) {
        return this.each(function(i) {
            $.popup.action(this, index);
        });
    };

    /**
     * Внутренний функционал
     */
    $.popup = {
        /**
         * Функция при открытии окна
         * @param el элемент
         */
        open: function(el, key, activator, opts) {
            var $el =$(el);
            var options = $el.data('popupOptions');
            options.key = key;
            options.activatorObj = activator;
	        if (typeof(opts)=='object' || typeof(opts)=='array') options = $.extend(options, opts);
            $el.data('popupOptions', options);
            // Выполнение обработчика открытия окна
            if (typeof(options.onOpen) == 'function') {
                var res = options.onOpen($el, options);
                switch (typeof(res)) {
                    case('object'):
                        options = $.extend(options, res);
                        $el.data('popupOptions', options);
                    break;
                    case('boolean'):
                        if (!res) return;
                    break;
                }
            }
            // Работа в ajax режиме
            switch(options.ajaxMode){
                case(false): // ajax выключен
                    if (options.modal) {
                        $("<div class='"+options.overlayClass+"'></div>").prependTo(document.body).fadeTo('normal',options.overlayOpacity);
                    }
                    $.popup.show($el);
                break;
                case('json'): // Ответ от сервера в виде объекта
                    $("<div class='"+options.waitingClass+"'></div>").prependTo(document.body);
                    if (options.modal) {
                        $("<div class='"+options.overlayClass+"'></div>").prependTo(document.body).fadeTo('normal',options.overlayOpacity);
                    };
                    $.ajax({
	                    'url' :         options.ajaxUrl,
	                    'data' :        options.ajaxData,
	                    'dataType' :    'json',
	                    'success' :     function(data){
                            $.popup.onAjax($el, data);
                        },
	                    'error' :       function (jqXHR, textStatus, errorThrown) {
		                    $.popup.hide($el);
		                    alert(textStatus);
	                    }
                    });
                break;
                case('content'): // Ответ от сервера в режиме html
                    $("<div class='"+options.waitingClass+"'></div>").prependTo(document.body);
                    if (options.modal) {
                        $("<div class='"+options.overlayClass+"'></div>").prependTo(document.body).fadeTo('normal',options.overlayOpacity);
                    };
                    $.get(options.ajaxUrl, options.ajaxData, function(data){
                        $el.find('.'+options.contentClass).html(data);

                    });
                    $.popup.show($el);
                break;
            };
        },
        /**
         * Функция для отображения окна
         * @param el окно
         */
        show: function(el){
            var $el = $(el);
            var options = $el.data('popupOptions');
            if (options.width) {
                $el.find('.'+options.contentClass).width(options.width);
                $el.width(options.width + 62);
            }
	        var wh = $(window).height();
	        var ww = $(window).width();
	        var w = $el.width();
			var h = $el.height();
			$el.css({
				marginLeft: - w/2,
				marginTop: - h/2
			});
	        if (wh<h) $el.css({
				    marginTop: 0,
				    position: 'absolute',
		            top: $(document).scrollTop()
			    });
	        if (ww<w) $el.css({
				    marginLeft: 0,
				    position: 'absolute',
		            left: $(document).scrollLeft()
			    });
            $('.'+options.waitingClass).remove();
            $el.show();
        },
        /**
         * Обработчик ответа от сервера в режиме ajax
         * @param el элемент диалогового окна
         * @param data данные от сервера
         */

        onAjax: function(el, data){
            var $el= $(el);
            var options = $el.data('popupOptions');
            $('.'+options.waitingClass).remove();
            if (typeof(options.onAjaxLoad) == 'function') {
                res = options.onAjaxLoad($el, data, options);
                switch (typeof(res)) {
                    case('object'):
                        options = $.extend(options, res);
                        $el.data('popupOptions', options);
                    break;
                    case('boolean'):
                        if (!res) {
                            if (options.modal) $('.'+options.overlayClass).fadeOut(function(){$(this).remove();});
                            return;
                        }
                    break;
                };
            };
           $.popup.show($el);

        },
        /**
         * Функция при закрытии окна
         * @param el элемент
         * @param success нажата ли кнопка действия
         */
        close: function(el, success) {
            var $el =$(el);
            var options = $el.data('popupOptions');
            var hide = true;

            if (typeof(options.onClose) == 'function') {
                var res = options.onClose($el, options.key, options);
                switch (typeof(res)) {
                    case('object'):
                        options = $.extend(options, res);
                        $el.data('popupOptions', options);
                    break;
                    case('boolean'):
                       hide = hide && res;
                    break;
                }
            }
            if (!success && typeof(options.onCancel) == 'function') {
                res = options.onCancel($el, options.key, options);
                switch (typeof(res)) {
                    case('object'):
                        options = $.extend(options, res);
                        $el.data('popupOptions', options);
                    break;
                    case('boolean'):
                       hide = hide && res;
                    break;
                }
            }
            if (hide) {
                $el.hide();
                if (options.modal) $('.'+options.overlayClass).fadeOut(function(){$(this).remove();});
            }
        },
        /**
         * Функция при нажатии кнопки действия
         * @param el элемент
         * @param index индекс кнопки
         */
        action: function(el, index) {
            var $el =$(el);
            var options = $el.data('popupOptions');
            if (typeof(options.onAction) == 'function') {
                res = options.onAction($el, index, options.key, options);
                switch (typeof(res)) {
                    case('object'):
                        options = $.extend(options, res);
                        $el.data('popupOptions', options);
                    break;
                    case('boolean'):
                        if (!res) return;
                    break;
                };
            };
            if (options.hideOnAction) $.popup.close($el, true);
        }
    }
})(jQuery);
