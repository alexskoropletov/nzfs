( function( $ ) {
    var imgList = [];
    var elements = [ "money", "money", "money_big", "money", "money", "money_big", "cristal", "cristal", "cristal_big", "energy", "energy", "energy_big", "energy", "energy", "energy_big" ];
    $.extend( {
        preload: function( imgArr, option ) {
            var setting = $.extend( {
                init: function( loaded, loaded_percent, total ) {},
                loaded: function( img, loaded, loaded_percent, total ) {},
                loaded_all: function( loaded, loaded_percent, total ) {}
            }, option );
            var total = imgArr.length;
            var one_percent = total / 100;
            var loaded_percent = 0;
            var loaded = 0;
            setting.init( 0, 0, total );
            for( var i in imgArr ) {
                imgList.push( $( "<img />" ).attr( "src", imgArr[i] ).load( function() {
                    loaded++;
                    loaded_percent = Math.ceil( 4 * ( loaded / one_percent ) );
                    setting.loaded( this, loaded, loaded_percent, total );
                    if( loaded == total ) {
                        setting.loaded_all( loaded, loaded_percent, total );
                    }
                } ) );
            }
        }
    } );
    $( "#info_window_hide" ).click( function() { $( "#info_window" ).hide() } );
    $( document ).ready( function() {
        $( "#info_window" ).hide();
        $( "#info_window" ).css( "display", "" );
        $( '.tooltip_element' ).tooltip( {
            bodyHandler: function() {
                return $( this ).find( ".tooltip" ).html();
            },
            showURL: false
        } );
        if( $( "#state_hash").size() ) {
            doThis( $( "#state_hash").attr( "value" ) );
        }
        $( ".main_menu_button" ).mouseover( function() {
            if( !$( this ).hasClass( "dm_current" ) ) {
                $( this ).addClass( "main_menu_button_hover" );
            }
        } );
        $( ".main_menu_button" ).mouseout( function() {
            if( !$( this ).hasClass( "dm_current" ) ) {
                $( this ).removeClass( "main_menu_button_hover" );
            }
        } );
        if( $( "#space_select").size() ) {
            $( ".space_level_menu_item" ).mouseover( function() {
                $( this ).addClass( "space_level_menu_item_active" );
            } );
            $( ".space_level_menu_item" ).mouseout( function() {
                $( this ).removeClass( "space_level_menu_item_active" );
            } );
        }
        if( $( "#hangar").size() ) {
            $( ".hangar_item_select" ).mouseover( function() {
                $( this ).addClass( "hangar_item_select_active" );
            } );
            $( ".hangar_item_select" ).mouseout( function() {
                $( this ).removeClass( "hangar_item_select_active" );
            } );
            $( ".ship_rotate_right" ).mouseover( function() {
                $( this ).addClass( "ship_rotate_right_active" );
            } );
            $( ".ship_rotate_right" ).mouseout( function() {
                $( this ).removeClass( "ship_rotate_right_active" );
            } );
            $( ".ship_rotate_left" ).mouseover( function() {
                $( this ).addClass( "ship_rotate_left_active" );
            } );
            $( ".ship_rotate_left" ).mouseout( function() {
                $( this ).removeClass( "ship_rotate_left_active" );
            } );
            $( "#ship_select" ).mouseover( function() {
                $( this ).find( ".ship_rotate_right" ).css( "opacity", 1 );
                $( this ).find( ".ship_rotate_left" ).css( "opacity", 1 );
            } );
            $( "#ship_select" ).mouseout( function() {
                $( this ).find( ".ship_rotate_left" ).css( "opacity", 0.3 );
                $( this ).find( ".ship_rotate_right" ).css( "opacity", 0.3 );
            } );
             $( ".item_rotate_right" ).mouseover( function() {
                $( this ).addClass( "item_rotate_right_active" );
            } );
            $( ".item_rotate_right" ).mouseout( function() {
                $( this ).removeClass( "item_rotate_right_active" );
            } );
            $( ".item_rotate_left" ).mouseover( function() {
                $( this ).addClass( "item_rotate_left_active" );
            } );
            $( ".item_rotate_left" ).mouseout( function() {
                $( this ).removeClass( "item_rotate_left_active" );
            } );
            $( ".ship_big_image" ).click( function() {
                $( "#hangar_info_image" ).html( "<img src='" + $( this).parent().find( ".ship_image_src").attr( "value" ) + "' border='0'>" );
                $( "#hangar_info_text" ).html(
                        "<h2>" + $( this).parent().find( ".ship_name").attr( "value" ) + "</h2>"
                    +   $( this).parent().find( ".ship_description").attr( "value" )
                    +   "<br clear='all'><div class='bar health_bar'><div class='text'>" + $( this).parent().find( ".ship_health" ).attr( "value" ) + "</div></div>"
                    +   "<div class='bar defense_bar'><div class='text'>" + $( this).parent().find( ".ship_defense" ).attr( "value" ) + "</div></div>"
                    +   "<div class='bar weapon_bar'><div class='text'>" + $( this).parent().find( ".ship_attack" ).attr( "value" ) + "</div></div>"
                    +   "<br clear='all'><div class='bar price_" + $( this).parent().find( ".ship_price_type" ).attr( "value" ) + "_bar'><div class='text'>" + $( this).parent().find( ".ship_price" ).attr( "value" ) + "</div></div>"
                );
            } );
            $( ".item_image" ).click( function() {
                $( "#hangar_info_image" ).html( "<img src='" + $( this ).parent().find( ".item_image_src" ).attr( "value" ) + "' border='0'>" );
                $( "#hangar_info_text" ).html(
                        "<h2>" + $( this ).parent().find( ".item_name" ).attr( "value" ) + "</h2>"
                    +   $( this ).parent().find( ".item_description" ).attr( "value" )
                    +   "<br clear='all'><div class='bar " + $( this ).parent().find( ".item_type" ).attr( "value" ) + "_bar'><div class='text'>" + $( this ).parent().find( ".item_modifier" ).attr( "value" ) + "</div></div>"
                    +   "<br clear='all'><div class='bar price_" + $( this ).parent().find( ".item_price_type" ).attr( "value" ) + "_bar'><div class='text'>" + $( this ).parent().find( ".item_price" ).attr( "value" ) + "</div></div>"
                );
            } );
            for( var i = 0; i < hangarScrollEquip.length; i++ ) {
                $( "#" + hangarScrollEquip[i] + "_scroll" ).jcarousel( { itemFallbackDimension: 100, scroll: 1, scrollType: hangarScrollEquip[i],initCallback: equip_initCallback, buttonNextHTML: null, buttonPrevHTML: null } );
            }
            $( "#ship_scroll" ).jcarousel( { itemFallbackDimension: 400, scroll: 1, initCallback: ship_initCallback, buttonNextHTML: null, buttonPrevHTML: null } );
        }
        if( $( "#preload_images").size() ) {
            VK.init( function( ) {
                VK.api( 'isAppUser', {}, function( data ) {
                    if( data.response == 1 ) {
                        $.preload( imageList , {
                            init: function( loaded, loaded_percent, total ) {
                                $( "#preload_marker" ).css( "width", loaded_percent + "px" );
                            },
                            loaded: function(img, loaded, loaded_percent, total) {
                                $( "#preload_marker" ).css( "width", loaded_percent + "px" );
                            },
                            loaded_all: function( loaded, loaded_percent, total ) {
                                VK.api( 'getUserInfoEx', { format: 'JSON', test_mode: 1 }, function( data ) {
                                    $.post( "/+/player/getPlayer", { userdata: data.response }, function( data ) {
                                        if( data.length ) {
                                            $( "#preload_marker" ).css( "width", loaded_percent + "px" );
                                            document.location.href = data;
                                        }
                                    } );
                                } );
                            }
                        } );
                    }
                } );
            } );
        }
    } );
//        function runEffect() {
////            $( "#effect" ).show( "scale", { percent: 100 }, 500 );
////            $( '#authorize-form' ).popupOpen();
//        };
//        $( "#button" ).click(function() {
//            runEffect();
//            return false;
//        });
//        $( document ).click( function() { $( "#effect" ).hide() } );
//        $( document ).ready( function() {
//            $( "#effect" ).hide();
//
//        } );
//        $( '#authorize-form' ).popup( '#authorize_link', {
//                    width: 530
//            } );
//        $( document ).ready( function() {
//            $( "#roulet_table td" ).each( function( key, val ) {
//                $( val ).find( ".bottom" ).css( "opacity", 0 );
//                $( val ).find( ".top" ).addClass( elements[ Math.round( Math.random() * ( elements.length - 2 ) ) ] ).css( "opacity", 1 );
//            } );
//        } );
} ) ( jQuery );

function runEffect( sContent ) {
//    $( "#info_window").popupOpen();
    $( "#info_window" ).find( ".info_window_text" ).html( sContent );
    $( "#info_window" ).show( "scale", { percent: 100, direction: 'horizontal' }, 250 );
};

function rotate_roulet( timetoroll, start ) {
    $( "#roulet_table td" ).each( function( key, val ) {
        if( start == "bottom" ) {
            $( val ).find( ".top" ).animate( { "opacity": 0 }, timetoroll );
            for( var i = 0; i < elements.length; i++ ) {
                $( val ).find( ".top" ).removeClass( elements[i] );
                $( val ).find( ".bottom" ).removeClass( elements[i] );
            }
            $( val ).find( ".bottom" ).addClass( elements[ Math.round( Math.random() * ( elements.length - 2 ) ) ] );
            $( val ).find( ".bottom" ).animate( { "opacity": 1 }, timetoroll );
        } else {
            $( val ).find( ".bottom" ).animate( { "opacity": 0 }, timetoroll );
            for( var i = 0; i < elements.length; i++ ) {
                $( val ).find( ".top" ).removeClass( elements[i] );
                $( val ).find( ".bottom" ).removeClass( elements[i] );
            }
            $( val ).find( ".top" ).addClass( elements[ Math.round( Math.random() * ( elements.length - 2 ) ) ] );
            $( val ).find( ".top" ).animate( { "opacity": 1 }, timetoroll );
        }
    } );
    timetoroll += 20;
    start = start == "top" ? "bottom" : "top";
    if( timetoroll > 500 ) {
        get_roulet_result();
        return true;
    } else {
        setTimeout( "rotate_roulet( " + timetoroll + ", '" + start + "' )", timetoroll );
    }
}
function get_roulet_result() {
    var res = [];
    $( "#roulet_table td" ).each( function( key, val ) {
        res[ res.length ] = $( val ).find( ".bottom" ).attr( "class" ).replace( "roulet bottom ", "" );
    } );
}

function equip_initCallback( carousel ) {
    $( '.item_rotate_left_' + carousel.options.scrollType ).bind( 'click', function() {
        carousel.prev();
        if( carousel.first == 1 ) {
            $( '.item_rotate_left_' + carousel.options.scrollType ).addClass( "hide" );
        } else {
            $( '.item_rotate_right_' + carousel.options.scrollType ).removeClass( "hide" );
        }
        $( "#hangar_info_image" ).html( "<img src='" + $( "#" + carousel.options.scrollType + "_scroll li" ).eq( carousel.first - 1 ).find( ".item_image_src" ).attr( "value" ) + "' border='0'>" );
        $( "#hangar_info_text" ).html(
                "<h2>" + $( "#" + carousel.options.scrollType + "_scroll li" ).eq( carousel.first - 1 ).find( ".item_name" ).attr( "value" ) + "</h2>"
            +   $( "#" + carousel.options.scrollType + "_scroll li" ).eq( carousel.first - 1 ).find( ".item_description" ).attr( "value" )
            +   "<br clear='all'><div class='bar " + carousel.options.scrollType + "_bar'><div class='text'>" + $( "#" + carousel.options.scrollType + "_scroll li" ).eq( carousel.first - 1 ).find( ".item_modifier" ).attr( "value" ) + "</div></div>"
            +   "<br clear='all'><div class='bar price_" + $( "#" + carousel.options.scrollType + "_scroll li" ).eq( carousel.first - 1 ).find( ".item_price_type" ).attr( "value" ) + "_bar'><div class='text'>" + $( "#" + carousel.options.scrollType + "_scroll li" ).eq( carousel.first - 1 ).find( ".item_price" ).attr( "value" ) + "</div></div>"
        );
        return false;
    } );
    $( '.item_rotate_right_' + carousel.options.scrollType ).bind( 'click', function() {
        carousel.next();
        if( carousel.last == $( "#hangar_items_" + carousel.options.scrollType + "_total_count" ).attr( "value" ) ) {
            $( '.item_rotate_right_' + carousel.options.scrollType ).addClass( "hide" );
        } else {
            $( '.item_rotate_left_' + carousel.options.scrollType ).removeClass( "hide" );
        }
        $( "#hangar_info_image" ).html( "<img src='" + $( "#" + carousel.options.scrollType + "_scroll li" ).eq( carousel.last - 1 ).find( ".item_image_src" ).attr( "value" ) + "' border='0'>" );
        $( "#hangar_info_text" ).html(
                "<h2>" + $( "#" + carousel.options.scrollType + "_scroll li" ).eq( carousel.last - 1 ).find( ".item_name" ).attr( "value" ) + "</h2>"
            +   $( "#" + carousel.options.scrollType + "_scroll li" ).eq( carousel.last - 1 ).find( ".item_description" ).attr( "value" )
            +   "<br clear='all'><div class='bar " + carousel.options.scrollType + "_bar'><div class='text'>" + $( "#" + carousel.options.scrollType + "_scroll li" ).eq( carousel.last - 1 ).find( ".item_modifier" ).attr( "value" ) + "</div></div>"
            +   "<br clear='all'><div class='bar price_" + $( "#" + carousel.options.scrollType + "_scroll li" ).eq( carousel.last - 1 ).find( ".item_price_type" ).attr( "value" ) + "_bar'><div class='text'>" + $( "#" + carousel.options.scrollType + "_scroll li" ).eq( carousel.last - 1 ).find( ".item_price" ).attr( "value" ) + "</div></div>"
        );
        return false; } );
}

function ship_initCallback( carousel ) {
    $( '.ship_rotate_left' ).bind( 'click', function() {
        carousel.prev();
        if( carousel.first == 1 ) {
            $( ".ship_rotate_left" ).addClass( "hide" );
        } else {
            $( ".ship_rotate_right" ).removeClass( "hide" );
        }
        $( "#hangar_info_image" ).html( "<img src='" + $( ".ship_big_image" ).eq( carousel.last - 1 ).parent().find( ".ship_image_src" ).attr( "value" ) + "' border='0'>" );
        $( "#hangar_info_text" ).html(
                "<h2>" + $( ".ship_big_image" ).eq( carousel.last - 1 ).parent().find( ".ship_name" ).attr( "value" ) + "</h2>"
            + $( ".ship_big_image" ).eq( carousel.last - 1 ).parent().find( ".ship_description" ).attr( "value" )
            +   "<br clear='all'><div class='bar health_bar'><div class='text'>" + $( ".ship_big_image" ).eq( carousel.last - 1 ).parent().find( ".ship_health" ).attr( "value" ) + "</div></div>"
            +   "<div class='bar defense_bar'><div class='text'>" + $( ".ship_big_image" ).eq( carousel.last - 1 ).parent().find( ".ship_defense" ).attr( "value" ) + "</div></div>"
            +   "<div class='bar weapon_bar'><div class='text'>" + $( ".ship_big_image" ).eq( carousel.last - 1 ).parent().find( ".ship_attack" ).attr( "value" ) + "</div></div>"
            +   "<br clear='all'><div class='bar price_" + $( ".ship_big_image" ).eq( carousel.last - 1 ).parent().find( ".ship_price_type" ).attr( "value" ) + "_bar'><div class='text'>" + $( ".ship_big_image" ).eq( carousel.last - 1 ).parent().find( ".ship_price" ).attr( "value" ) + "</div></div>"
        );
        return false;
    } );
    $( '.ship_rotate_right' ).bind( 'click', function() {
        carousel.next();
        if( carousel.last == $( "#ships_total_count" ).attr( "value" ) ) {
            $( ".ship_rotate_right" ).addClass( "hide" );
        } else {
            $( ".ship_rotate_left" ).removeClass( "hide" );
        }
        $( "#hangar_info_image" ).html( "<img src='" + $( ".ship_big_image" ).eq( carousel.last - 1 ).parent().find( ".ship_image_src" ).attr( "value" ) + "' border='0'>" );
        $( "#hangar_info_text" ).html(
                "<h2>" + $( ".ship_big_image" ).eq( carousel.first - 1 ).parent().find( ".ship_name" ).attr( "value" ) + "</h2>"
            + $( ".ship_big_image" ).eq( carousel.first - 1 ).parent().find( ".ship_description" ).attr( "value" )
            +   "<br clear='all'><div class='bar health_bar'><div class='text'>" + $( ".ship_big_image" ).eq( carousel.first - 1 ).parent().find( ".ship_health" ).attr( "value" ) + "</div></div>"
            +   "<div class='bar defense_bar'><div class='text'>" + $( ".ship_big_image" ).eq( carousel.first - 1 ).parent().find( ".ship_defense" ).attr( "value" ) + "</div></div>"
            +   "<div class='bar weapon_bar'><div class='text'>" + $( ".ship_big_image" ).eq( carousel.first - 1 ).parent().find( ".ship_attack" ).attr( "value" ) + "</div></div>"
            +   "<br clear='all'><div class='bar price_" + $( ".ship_big_image" ).eq( carousel.first - 1 ).parent().find( ".ship_price_type" ).attr( "value" ) + "_bar'><div class='text'>" + $( ".ship_big_image" ).eq( carousel.first - 1 ).parent().find( ".ship_price" ).attr( "value" ) + "</div></div>"
        );
        return false;
    } );
}

function setEquipOn( aSet ) {
    $( "#" + aSet.type + "_scroll").find( ".item_image" ).each( function( key, val ) {
        if( $( val ).find( ".buy" ).hasClass( "hide_none" ) ) {
            if( !$( val ).find( ".is_on" ).hasClass( "hide_none" ) ) {
                $( val ).find( ".is_on" ).addClass( "hide_none" );
            }
            if( $( val ).find( ".set_on" ).hasClass( "hide_none" ) ) {
                $( val ).find( ".set_on" ).removeClass( "hide_none" );
            }
        }
    } );
    $( "#item_" + aSet.id + " .set_on" ).addClass( "hide_none" );
    $( "#item_" + aSet.id + " .is_on" ).removeClass( "hide_none" );
    doThis( $( "#state_hash").attr( "value" ) );
}

function cheater( aSet ) {
    runEffect( "Попался!" );
}

function setShipOn( aSet ) {
    $( "#ship_scroll" ).find( ".ship_is_on" ).each( function( key, val ) {
        if( !$( val ).find( ".is_on_text" ).hasClass( "hide" ) ) {
            $( val ).find( ".set_on_text" ).removeClass( "hide" );
            $( val ).find( ".is_on_text" ).addClass( "hide" );
        }
    } );
    $( "#ship_" + aSet.id ).find( ".is_on_text" ).removeClass( "hide" );
    $( "#ship_" + aSet.id ).find( ".set_on_text" ).addClass( "hide" );
    doThis( $( "#state_hash").attr( "value" ) );
}

function boughtShip( aSet ) {
    $( "#ship_scroll" ).find( ".ship_is_on" ).each( function( key, val ) {
        if( $( val ).find( ".buy" ).hasClass( "hide" ) ) {
            $( val ).find( ".is_on_text" ).addClass( "hide" );
            $( val ).find( ".set_on_text" ).removeClass( "hide" );
        }
    } );
    $( "#ship_" + aSet.id + " .buy" ).addClass( "hide" );
    $( "#ship_" + aSet.id ).find( ".is_on_text" ).removeClass( "hide" );
    doThis( $( "#state_hash").attr( "value" ) );
}

function cantAfford( aSet ) {
    if( aSet['type'] == 'money' ) {
        runEffect( "Недостаточно кредитов!" );
    } else {
        runEffect( "Недостаточно кристаллов!" );
    }
}

function doThis( hash ) {
    VK.api( 'getUserInfo', { format: 'JSON' }, function( data ) {
        $.post( "/dev.php/+/main/do", { pid: $( "#pid").attr( "value" ), vkdata: data, hash: hash }, function( data ) {
            console.log( data );
            var pr = JSON.parse( data );
            eval( pr['action'] + "(" + JSON.stringify( pr.set ) + ")" );
        } );
    } );
}

function boughtEquip( aSet ) {
    $( "#item_" + aSet.id + " .buy" ).addClass( "hide_none" );
    $( "#" + aSet.type + "_scroll").find( ".item_image" ).each( function( key, val ) {
        if( $( val ).find( ".buy" ).hasClass( "hide_none" ) ) {
            if( !$( val ).find( ".is_on" ).hasClass( "hide_none" ) ) {
                $( val ).find( ".is_on" ).addClass( "hide_none" );
            }
            if( $( val ).find( ".set_on" ).hasClass( "hide_none" ) ) {
                $( val ).find( ".set_on" ).removeClass( "hide_none" );
            }
        }
    } );
    $( "#item_" + aSet.id + " .set_on" ).addClass( "hide_none" );
    $( "#item_" + aSet.id + " .is_on" ).removeClass( "hide_none" );
    doThis( $( "#state_hash").attr( "value" ) );
}

function showPlayerState( aSet ) {
    for( k in aSet ) {
        if( $( "#" + k ).size() ) {
            $( "#" + k ).find( ".bar_text" ).text( aSet[ k ] );
        }
    }
}

function attack( aSet ) {
    if( aSet.victory == 1 ) {
        runEffect( "Победа!!!" );
        if( aSet.level_up ) {
            runEffect( "Вы достигли " + aSet.level_up + " уровня!" );
        }
        if( aSet.medal ) {
            runEffect( "Вы заработали медаль " + aSet.medal.name + "!<br>" + aSet.bonus + "<img src='" + aSet.medal.image + "' border='0' title='" + aSet.medal.name + "'>" );
        }
    } else {
        runEffect( "Поражение!" );
    }
    doThis( $( "#state_hash").attr( "value" ) );
}
function dig( sUrl, iEquipId, iEventId ) {
    coverShowHide();
    $( "#space_dig" ).parent().css( "display", "none" );
    $.post( sUrl, { iEquipId: iEquipId, iEventId: iEventId, iPlayerId: $( "#player_id" ).attr( "value" ) }, function( data ) {
        var parsed = JSON.parse( data );
        $( "#exp_bar" ).find( ".bar_text" ).text( parsed.exp );
        $( "#money_bar" ).find( ".bar_text" ).text( parsed.money );
        $( "#energy_bar" ).find( ".bar_text" ).text( parsed.energy );
        $( "#health_bar" ).find( ".bar_text" ).text( parsed.health );
        $( "#defense_bar" ).find( ".bar_text" ).text( parsed.defense );
        $( "#attack_bar" ).find( ".bar_text" ).text( parsed.attack );
        $( "#player_level" ).find( ".level_text" ).text( parsed.level );
        $( "#space_event" ).css( "display", "none" );
        runEffect( "Вы нашли ресурс" );
        if( parsed.medal ) {
            runEffect( "Вы заработали медаль " + parsed.medal.name + "!<br>" + parsed.bonus + "<img src='" + parsed.medal.image + "' border='0' title='" + parsed.medal.name + "'>" );
        }
        if( parsed.level_up ) {
            runEffect( "Вы достигли " + parsed.level_up + " уровня! Поздравляем!" );
        }
    } );
}

function notenoughenergy() {
    runEffect( "Не хватает энергии" );
}

function coverShowHide() {
    $( "#cover").css( "display", ( $( "#cover").css( "display" ) == "none" ? "" : "none" ) );
}