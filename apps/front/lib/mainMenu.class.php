<?php
class mainMenu extends dmMenu {
    public function getDefaultOptions() {
        return array(
            'ul_class'          => null,
            'li_class'          => null,
            'show_id'           => false,
            'show_children'     => true,
            'translate'         => true
        );
    }

    public function getName() {
        return $this->name;
    }

    public function getLabel() {
        return null === $this->label ? $this->getName() : "<div class='main_menu_button_span'><img src='/theme/images/menu_". $this->label .".png' border='0'></div>";
//        return null === $this->label ? $this->getName() : $this->label;
    }

    public function getLink() {
        
        if( !$this->link instanceof dmBaseLinkTag && !empty( $this->link ) ) {
            $this->link = $this->helper->link( $this->link );
        }
        return $this->link;
    }

    public function render() {
        $html = '';
        if ( $this->checkUserAccess() && $this->hasChildren() ) {
            $html = $this->renderUlOpenTag();
            foreach( $this->children as $child ) {
                $html .= $child->renderChild();
            }
            $html .= '</ul>';
        }
        return $html;
    }

    protected function renderUlOpenTag() {
        $class  = $this->getOption( 'ul_class' );
        $id     = $this->getOption( 'show_id' ) ? dmString::slugify( $this->name.'-menu' ) : null;
        return '<ul'.($id ? ' id="'.$id.'"' : '').($class ? ' class="'.$class.'"' : '').'>';
    }

    protected function renderLiOpenTag() {
        $classes  = array();
        $id       = $this->getOption('show_id') ? dmString::slugify( 'menu-'.$this->getRoot()->getName() . '-' . $this->getName() ) : null;
        $link     = $this->getLink();
        if( $this->isFirst() ) {
            $classes[] = 'first';
        }
        if( $this->isLast() ) {
            $classes[] = 'last';
        }
        if( $this->getOption( 'li_class' ) ) {
            $classes[] = $this->getOption( 'li_class' );
        }
        if( ( $link && $link->isCurrent() ) ) {
            $classes[] = $link->getOption( 'current_class' );
        } elseif( $link && $link->isParent() ) {
            $classes[] = $link->getOption( 'parent_class' );
        }
        return '<li ' .($id ? ' id="'.$id.'"' : '').(!empty($classes) ? ' class="'.implode(' ', $classes).'"' : '').'>';
    }
}