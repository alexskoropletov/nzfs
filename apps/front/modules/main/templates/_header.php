<div id='info_window' class='popupWindow'>
    <div class='info_window_text'></div>
    <div id='info_window_hide' class='popupClose'>Ок</div>
</div>

<div class='tooltip_element header_bar' id='energy_bar'>
    <span class='tooltip'>Энегрия</span>
    <div class="bar_text">
        <?php echo $aResult['energy']?>
    </div>
</div>
<div class='tooltip_element header_bar' id='super_money_bar'>
    <span class='tooltip'>Кристаллы</span>
    <div class="bar_text">
        <?php echo $aResult['super_money']?>
    </div>
</div>
<div class='tooltip_element header_bar' id='money_bar'>
    <span class='tooltip'>Кредиты</span>
    <div class="bar_text">
        <?php echo $aResult['money']?>
    </div>
</div>
<div class='tooltip_element header_bar' id='exp_bar'>
    <span class='tooltip'>Опыт</span>
    <div class="bar_text">
        <?php echo $aResult['exp']?>
    </div>
</div>

<div id='health_bar' class='status_bar tooltip_element'>
    <span class='tooltip'>Здоровье</span>
    <span class="bar_text">
        <?php echo $aResult['health']?>
    </span>
</div>
<div id='defense_bar' class='status_bar tooltip_element'>
    <span class='tooltip'>Броня</span>
    <span class="bar_text">
        <?php echo $aResult['defense']?>
    </span>
</div>
<div id='attack_bar' class='status_bar tooltip_element'>
    <span class='tooltip'>Атака</span>
    <span class="bar_text">
        <?php echo $aResult['attack']?>
    </span>
</div>

<div id='player_image' class='tooltip_element'>
    <?php echo _media( $oPlayerImage )->size( 69, 55 ); ?>
    <span class='tooltip'><?php echo $sPlayerName?></span>
</div>
<div id='player_name'>
    <span class="bar_text"></span>
</div>
<div id='player_level' class='tooltip_element'>
    <div class="level_text"><?php echo $aResult['level']?></div>
    <span class='tooltip'>Уровень игрока</span>
</div>
<input type='hidden' id='state_hash' value='<?php echo $sGetStatesHash?>'>
