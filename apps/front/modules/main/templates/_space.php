<div id='space' style='background: url( <?php echo _media( $oBackgroundImage )->getSrc() ?> ) 0 0 no-repeat;'>
    <?php if( isset( $oEvent ) ) { ?>
        <div id='space_event' class='tooltip_element'>
            <span class='tooltip'>
                <h2>Перед тобой <?php echo $oEvent->getName()?></h2>
                <?php echo $oEvent->getDescription()?><br>
                <?php if( in_array( $oEvent->getType(), array( 'enemy', 'enemy_boss' ) ) ) {?>
                    Атака: <?php echo $oEvent->getAttack()?><br>
                    Здоровье: <?php echo $oEvent->getHealth()?><br>
                    Защита: <?php echo $oEvent->getDefense()?><br>
                    <?php if( $oEvent->getType() == 'enemy_boss' ) {?>
                        Это босс!!!
                    <?php } ?>
                <?php } ?>
            </span>
            <?php echo _media( $oEvent->getImage() )?><br>
        </div>
    <?php } ?>
    <a href='<?php echo _link( 'main/kosmosVyiborUrovnya' )->param( "level", $iLevel )->title( "Дальше" )->text( "Дальше" )->getHref() ?>'>
        <div id='space_go' class='space_button'><div class='text'>Дальше</div></div></a>
    <?php if( isset( $oEvent ) ) { ?>
        <?php if( in_array( $oEvent->getType(), array( 'enemy', 'enemy_boss' ) ) ) {?>
            <div id='space_fight' onclick='javascript: $( this ).hide(); doThis( "<?php echo $sAttackHash?><?php echo $oEvent->getId() ?>" ); ' class='space_button'><div class='text'>Сражаться</div></div></a>
        <?php } else {?>
            <div id='space_dig' onclick='javascript: $( this ).hide(); doThis( "<?php echo $sDigHash?><?php echo $oEvent->getId() ?>" );' class='space_button'><div class='text'>Исследовать</div></div></a>
        <?php } ?>
    <?php } ?>
</div>