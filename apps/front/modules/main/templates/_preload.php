<div id='preload_images'>
    <script type="text/javascript">
        var imageList = [
            "<?php echo implode( '","', $aImages ) ?>",
            "/theme/images/ajax-loader.gif",
            "/theme/images/attack_bar.png",
            "/theme/images/background.jpg",
            "/theme/images/buy.png",
            "/theme/images/buy_small.png",
            "/theme/images/check.png",
            "/theme/images/check_small.png",
            "/theme/images/coverdot.png",
            "/theme/images/defense_bar.png",
            "/theme/images/defense_buy.png",
            "/theme/images/defense_is_on.png",
            "/theme/images/defense_set_on.png",
            "/theme/images/energy.png",
            "/theme/images/exp.png",
            "/theme/images/hangar_item_select.png",
            "/theme/images/hangar_item_select_active.png",
            "/theme/images/hangar_robot_select.png",
            "/theme/images/health_bar.png",
            "/theme/images/health_buy.png",
            "/theme/images/health_is_on.png",
            "/theme/images/health_set_on.png",
            "/theme/images/indicator_attack.png",
            "/theme/images/indicator_defence.png",
            "/theme/images/indicator_health.png",
            "/theme/images/inform_window.png",
            "/theme/images/level.png",
            "/theme/images/loading_big.jpg",
            "/theme/images/main_menu_button.png",
            "/theme/images/main_menu_button_active.png",
            "/theme/images/main_menu_button_hover.png",
            "/theme/images/menu_hangar.png",
            "/theme/images/menu_settings.png",
            "/theme/images/menu_space.png",
            "/theme/images/money.png",
            "/theme/images/popup.png",
            "/theme/images/rotate.png",
            "/theme/images/rotate_active.png",
            "/theme/images/rotate_mini.png",
            "/theme/images/rotate_mini_active.png",
            "/theme/images/ship_buy.png",
            "/theme/images/ship_is_on.png",
            "/theme/images/ship_set_on.png",
            "/theme/images/space_button.png",
            "/theme/images/space_menu_highlight.png",
            "/theme/images/super_money.png",
            "/theme/images/top_panel.png",
            "/theme/images/uncheck.png",
            "/theme/images/uncheck_small.png",
            "/theme/images/weapon_buy.png",
            "/theme/images/weapon_is_on.png",
            "/theme/images/weapon_set_on.png"
        ];
    </script>
    <div id="preload_indicator">
        <div id="preload_marker"></div>
    </div>
</div>
<input type='hidden' id='go' value='<?php echo $sGo ?>'>

