<div id='hangar'>
    <input type='hidden' id='be_hash' value='<?php echo $sBuyEquipHash?>'>
    <input type='hidden' id='eo_hash' value='<?php echo $sEquipOnHash?>'>
    <script language="javascript">
        var hangarScrollEquip = [];
    </script>
    <div id='ship_select'>
        <div class='ship_rotate_left hide'></div>
        <div class='ship_rotate_right'></div>
        <div id='ship_image'>
            <ul id="ship_scroll">
                <?php foreach( $aAllShips as $aShip ) {?>
                    <li id='ship_<?php echo $aShip['id']?>'>
                        <div class='ship_big_image'>
                            <?php echo _media( $aShip['image'] )?>
                        </div>
                        <div class='ship_is_on'>
                            <span class='is_on_text tooltip_element <?php if( !$aShip['is_on'] || !in_array( $aShip['id'], $aPlayerShips ) ) {?>hide<?php }?>'><span class='tooltip'>Корабль выбран</span><div class='unequip_ship'></div></span>
                            <span class='set_on_text tooltip_element <?php if( $aShip['is_on'] || !in_array( $aShip['id'], $aPlayerShips ) ) {?>hide<?php }?>'><span class='tooltip'>Выбрать корабль</span><a href='javascript: void(0);' onclick='javascript: doThis( "<?php echo $sShipOnHash?><?php echo $aShip['id']?>" );'><div class='equip_ship'></div></a></span>
                            <span class='buy tooltip_element <?php if( in_array( $aShip['id'], $aPlayerShips ) ) {?>hide<?php }?>'><span class='tooltip'>Купить корабль</span><a href='javascript: void(0);' onclick='javascript: doThis( "<?php echo $sBuyShipHash?><?php echo $aShip['id']?>" );'><div class='buy_ship'></div></a></span>
                        </div>
                        <input type='hidden' class='ship_image_src' value='<?php echo _media( $aShip['image_small'] )->getSrc()?>'>
                        <input type='hidden' class='ship_name' value='<?php echo $aShip['name']?>'>
                        <input type='hidden' class='ship_description' value='<?php echo $aShip['description']?>'>
                        <input type='hidden' class='ship_price' value='<?php echo $aShip['price']?>'>
                        <input type='hidden' class='ship_price_type' value='<?php echo $aShip['price_type']?>'>
                        <input type='hidden' class='ship_health' value='<?php echo $aShip['health']?>'>
                        <input type='hidden' class='ship_defense' value='<?php echo $aShip['defense']?>'>
                        <input type='hidden' class='ship_attack' value='<?php echo $aShip['attack']?>'>
                    </li>
                <?php }?>
            </ul>
            <input type='hidden' id='ships_total_count' value='<?php echo count( $aAllShips );?>'>
        </div>
    </div>
    <div id='hangar_items_select'>
        <?php foreach( $aAllEquip as $sType => $aEquipOfType ) {?>
            <div class='hangar_item_select <?php echo $sType?>'>
                <div class='item_rotate_left hide item_rotate_left_<?php echo $sType?>'></div>
                <div class='item_rotate_right item_rotate_right_<?php echo $sType?>'></div>
                <div class='hangar_item'>
                    <ul id="<?php echo $sType?>_scroll">
                        <?php foreach( $aEquipOfType as $aEquip ) {?>
                            <li id='item_<?php echo $aEquip['id']?>'>
                                <div class='item_image' style='background: url( <?php echo _media( $aEquip['image_small'] )->getSrc() ?> ) 0 0 no-repeat;'>
                                    <span class='tooltip_element is_on <?php if( !$aEquip['is_on'] || !in_array( $aEquip['id'], $aPlayerEquip ) ) {?>hide_none<?php }?>'><span class='tooltip'>Оборудование установлено</span><div class='equip_item is_on_<?php echo $sType?>'></div></span>
                                    <span class='tooltip_element set_on <?php if( $aEquip['is_on'] || !in_array( $aEquip['id'], $aPlayerEquip ) ) {?>hide_none<?php }?>'><span class='tooltip'>Установить оборудование</span><div class='equip_item set_on_<?php echo $sType?>' onclick='javascript: doThis( "<?php echo $sBuyEquipHash?><?php echo $aEquip['id']?>" );'></div></span>
                                    <span class='tooltip_element buy <?php if( in_array( $aEquip['id'], $aPlayerEquip ) ) {?>hide_none<?php }?>'><span class='tooltip'>Купить оборудование</span><div class='equip_item buy_<?php echo $sType?>' onclick='javascript: doThis( "<?php echo $sBuyEquipHash?><?php echo $aEquip['id']?>" );'></div></span>
                                </div>
                                <input type='hidden' class='item_image_src' value='<?php echo _media( $aEquip['image'] )->getSrc()?>'>
                                <input type='hidden' class='item_name' value='<?php echo $aEquip['name']?>'>
                                <input type='hidden' class='item_description' value='<?php echo $aEquip['description']?>'>
                                <input type='hidden' class='item_price' value='<?php echo $aEquip['price']?>'>
                                <input type='hidden' class='item_price_type' value='<?php echo $aEquip['price_type']?>'>
                                <input type='hidden' class='item_modifier' value='<?php echo $aEquip['modifier']?>'>
                                <input type='hidden' class='item_type' value='<?php echo $sType?>'>
                            </li>
                        <?php }?>
                    </ul>
                    <input type='hidden' id='hangar_items_<?php echo $sType?>_total_count' value='<?php echo count( $aAllShips );?>'>
                </div>
            </div>
            <script language="javascript">
                hangarScrollEquip[ hangarScrollEquip.length ] = "<?php echo $sType?>";
            </script>
        <?php }?>
    </div>
    <div id='hangar_info'>
        <div id='hangar_info_image'>
            <?php echo _media( $oPlayerShip->getImageSmall() )?>
        </div>
        <div id='hangar_info_text'>
            <h2><?php echo $oPlayerShip->getName()?></h2>
            <?php echo $oPlayerShip->getDescription()?>
            <br clear='all'><div class='bar health_bar'><div class='text'><?php echo $oPlayerShip->getHealth()?></div></div>
            <div class='bar defense_bar'><div class='text'><?php echo $oPlayerShip->getDefense()?></div></div>
            <div class='bar weapon_bar'><div class='text'><?php echo $oPlayerShip->getAttack()?></div></div>
            <br clear='all'><div class='bar price_<?php echo $oPlayerShip->getPriceType()?>_bar'><div class='text'><?php echo $oPlayerShip->getPrice()?></div></div>
        </div>
        <input type='hidden' value='<?php echo $oPlayerShip->getId()?>' id='ship_id'>
        <input type='hidden' value='<?php echo $oPlayerShip->getDescription()?>' id='ship_description'>
        <input type='hidden' value='<?php echo $oPlayerShip->getName()?>' id='ship_name'>
        <input type='hidden' value='<?php echo $oPlayerShip->getHealth()?>' id='ship_health'>
        <input type='hidden' value='<?php echo $oPlayerShip->getDefense()?>' id='ship_defense'>
        <input type='hidden' value='<?php echo $oPlayerShip->getAttack()?>' id='ship_attack'>
        <input type='hidden' value='<?php echo _media( $oPlayerShip->getImageSmall() )->getSrc()?>' id='ship_small_image'>
        <input type='hidden' value='<?php echo _media( $oPlayerShip->getImage() )->getSrc()?>' id='ship_big_image'>
    </div>
</div>