<div id='space_select'>
    <div id='space_level_menu'>
        <?php foreach( $aSpaceLevels as $aSpaceLevel ) {?>
            <div class='space_level_menu_item'>
                <a href='<?php echo _link( 'main/kosmosVyiborUrovnya' )->param( "level", $aSpaceLevel['level'] )->title( $aSpaceLevel['name'] )->text( $aSpaceLevel['name'] )->getHref() ?>'>
                    <?php echo _media( $aSpaceLevel['image'] )?>
                    <div class='space_menu_item_name'><?php echo $aSpaceLevel['name']?></div>
                </a>
            </div>
        <?php }?>
    </div>
</div>
