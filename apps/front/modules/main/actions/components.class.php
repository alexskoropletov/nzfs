<?php
/**
 * Main components
 * 
 * No redirection nor database manipulation ( insert, update, delete ) here
 * 
 * 
 * 
 * 
 * 
 * 
 */
class mainComponents extends myFrontModuleComponents {

    public function executeHeader(dmWebRequest $oRequest) {
        if( $oRequest->getParameter( "uid" ) && $oRequest->getParameter( "pid" ) ) {
            if( $oUser = Doctrine::getTable( "DmUser" )->findOneByPlayer( $oRequest->getParameter( "pid" ) ) ) {
                $this->getUser()->signin( $oUser );
            }
        }
        if( $this->getUser()->isAuthenticated() ) {
            $this->oPlayerImage = $this->getUser()->getUser()->getPlayer()->getImageVk() ? $this->getUser()->getUser()->getPlayer()->getImageVk() : $this->getUser()->getUser()->getPlayer()->getImage();
            $this->sPlayerName = $this->getUser()->getUser()->getPlayer()->getName();
            $this->iPlayerId = $this->getUser()->getUser()->getPlayer()->getId();
            if( !$oPlayerState = Doctrine::getTable( "PlayerState" )->findOneByPlayer( $this->iPlayerId ) ) {
                $oPlayerState = new PlayerState();
                $oPlayerState->setPlayer( Doctrine::getTable( "Player" )->find( $this->iPlayerId ) );
                $oPlayerState->setLevel( Doctrine::getTable( "Level" )->find( 1 ) );
                $oPlayerState->save();
            }
            $oLevel = Doctrine::getTable( "Level" )->find( $oPlayerState->getLevel() );
            $this->aResult = array(
                "money"         => $oPlayerState->getMoney(),
                "super_money"   => $oPlayerState->getSuperMoney(),
                "exp"           => $oPlayerState->getXp() . " / " . $oLevel->getNextLevel(),
                "energy"        => $oPlayerState->getEnergy() . "/" . $oPlayerState->getMaximumEnergy(),
                "attack"        => $oPlayerState->getAttack(),
                "defense"       => $oPlayerState->getDefense(),
                "health"        => $oPlayerState->getCurrentHealth() . "/" . $oPlayerState->getHealth(),
                "level"         => $oPlayerState->getLevel(),
            );
            $this->sGetStatesHash = mainActions::getHashByAction( "gs" );
        }
    }

    public function executeFooter() {

    }

    public function executeHangar(dmWebRequest $oRequest) {
        if( $oRequest->getParameter( "uid" ) && $oRequest->getParameter( "pid" ) ) {
            if( $oUser = Doctrine::getTable( "DmUser" )->findOneByPlayer( $oRequest->getParameter( "pid" ) ) ) {
                $this->getUser()->signin( $oUser );
            }
        }
        if( $this->getUser()->isAuthenticated() ) {
            $this->iPlayerId    = $this->getUser()->getUser()->getPlayer()->getId();
            $aEquipOn           = array();
            $oAllEquip          = Doctrine::getTable( "Equip" )->findAll();
            $oAllShips          = Doctrine::getTable( "Ship" )->findAll();
            $this->aAllEquip    = array();
            $this->aAllShips    = array();
            $this->aPlayerShips = array();
            $this->aPlayerEquip = array();
            $oPlayerEquip       = Doctrine::getTable( "PlayerEquip" )->createQuery( "c" )->where( "c.player = ?", $this->getUser()->getUser()->getPlayer()->getId() )->execute();
            if( $oPlayerShip    = Doctrine::getTable( "PlayerShip" )->createQuery( "c" )->where( "c.player = ?", $this->getUser()->getUser()->getPlayer()->getId() )->andWhere( "c.is_on = 1" )->fetchOne() ) {
                $this->oPlayerShip = $oPlayerShip->getShip();
            } else {
                $this->oPlayerShip = Doctrine::getTable( "Ship" )->findAll()->getFirst();
            }
            $oPlayerShips       = Doctrine::getTable( "PlayerShip" )->createQuery( "c" )->where( "c.player = ?", $this->getUser()->getUser()->getPlayer()->getId() )->execute();
            foreach( $oPlayerShips as $oPlayerShip ) {
                $this->aPlayerShips[] = $oPlayerShip->getShip()->getId();
            }
            foreach( $oAllShips as $oShip ) {
                $this->aAllShips[] = array(
                    "id"            => $oShip->getId(),
                    "image"         => $oShip->getImage(),
                    "image_small"   => $oShip->getImageSmall(),
                    "price"         => $oShip->getPrice(),
                    "price_type"    => $oShip->getPriceType(),
                    "health"        => $oShip->getHealth(),
                    "defense"       => $oShip->getDefense(),
                    "attack"        => $oShip->getAttack(),
                    "is_on"         => ( $oShip->getId() == $this->oPlayerShip->getId() ? 1 : 0 ),
                    "name"          => $oShip->getName(),
                    "description"   => $oShip->getDescription()
                );
            }
            foreach( $oPlayerEquip as $oEquip ) {
                $this->aPlayerEquip[] = $oEquip->getEquip()->getId();
                if( $oEquip->getIsOn() ) {
                    $aEquipOn[ $oEquip->getEquip()->getType() ] = $oEquip->getEquip()->getId();
                }
            }
            foreach( $oAllEquip as $oEquip ) {
                $this->aAllEquip[ $oEquip->getType() ][] = array(
                    "id"            => $oEquip->getId(),
                    "image"         => $oEquip->getImage(),
                    "image_small"   => $oEquip->getImageSmall(),
                    "is_on"         => ( isset( $aEquipOn[ $oEquip->getType() ] ) && $oEquip->getId() == $aEquipOn[ $oEquip->getType() ] ? 1 : 0 ),
                    "name"          => $oEquip->getName(),
                    "description"   => $oEquip->getDescription(),
                    "price"         => $oEquip->getPrice(),
                    "price_type"    => $oEquip->getPriceType(),
                    "modifier"      => $oEquip->getModifier(),
                );
            }
            $this->sBuyShipHash = mainActions::getHashByAction( "bs" );
            $this->sBuyEquipHash = mainActions::getHashByAction( "be" );
            $this->sShipOnHash = mainActions::getHashByAction( "so" );
            $this->sEquipOnHash = mainActions::getHashByAction( "eo" );
        }
    }

    public function executePreload() {
        if( $this->getUser()->isAuthenticated() ) {
            $this->sGo = $this->getHelper()->link( 'main/angar' )->getHref();
        }
        $this->aImages = array();
        $oShips = Doctrine::getTable( "Ship" )->findAll();
        foreach( $oShips as $oShip ) {
            if( $oShip->getImage() ) {
                if( $this->getHelper()->media( $oShip->getImage() )->getSrc() ) {
                  $this->aImages[] = $this->getHelper()->media( $oShip->getImage() )->getSrc();
                }
            }
            if( $oShip->getImageSmall() ) {
                if( $this->getHelper()->media( $oShip->getImageSmall() )->getSrc() ) {
                    $this->aImages[] = $this->getHelper()->media( $oShip->getImageSmall() )->getSrc();
                }
            }
        }
        $oEquips = Doctrine::getTable( "Equip" )->findAll();
        foreach( $oEquips as $oEquip ) {
            if( $oEquip->getImage() ) {
                if( $this->getHelper()->media( $oEquip->getImage() )->getSrc() ) {
                    $this->aImages[] = $this->getHelper()->media( $oEquip->getImage() )->getSrc();
                }
            }
            if( $oEquip->getImageSmall() ) {
                if( $this->getHelper()->media( $oEquip->getImageSmall() )->getSrc() ) {
                    $this->aImages[] = $this->getHelper()->media( $oEquip->getImageSmall() )->getSrc();
                }
            }
        }
        $oSpaceLevels = Doctrine::getTable( "SpaceLevel" )->findAll();
        foreach( $oSpaceLevels as $oSpaceLevel ) {
            if( $oSpaceLevel->getImageMenu() ) {
                if( $this->getHelper()->media( $oSpaceLevel->getImageMenu() )->getSrc() ) {
                    $this->aImages[] = $this->getHelper()->media( $oSpaceLevel->getImageMenu() )->getSrc();
                }
            }
        }
        $oSpaceLevelsBG = Doctrine::getTable( "SpaceLevelBackground" )->findAll();
        foreach( $oSpaceLevelsBG as $oSpaceLevel ) {
            if( $oSpaceLevel->getImage() ) {
                if( $this->getHelper()->media( $oSpaceLevel->getImage() )->getSrc() ) {
                    $this->aImages[] = $this->getHelper()->media( $oSpaceLevel->getImage() )->getSrc();
                }
            }
        }
        $oSpaceObjects = Doctrine::getTable( "SpaceObject" )->findAll();
        foreach( $oSpaceObjects as $oSpaceObject ) {
            if( $oSpaceObject->getImage() ) {
                if( $this->getHelper()->media( $oSpaceObject->getImage() )->getSrc() ) {
                    $this->aImages[] = $this->getHelper()->media( $oSpaceObject->getImage() )->getSrc();
                }
            }
        }
    }

    public function executeSpace(dmWebRequest $oRequest) {
        if( $this->iLevel = $oRequest->getParameter( "level" ) ) {
            $oSpaceLevelBackgrounds = Doctrine::getTable( "SpaceLevelBackground" )->findBySpaceLevel( $this->iLevel );
            $aBackgrounds = array();
            foreach( $oSpaceLevelBackgrounds as $oSpaceLevelBackground ) {
                $aBackgrounds[] = $oSpaceLevelBackground->getImage();
            }
            shuffle( $aBackgrounds );
            $aSpaceEvents = array();
            $this->oBackgroundImage = $aBackgrounds[0];
            $oSpaceEvents = Doctrine::getTable( "SpaceEvents" )->findBySpaceLevel( $this->iLevel );
            $iSummChance = 1;
            foreach( $oSpaceEvents as $oSpaceEvent ) {
                $iSummChance += $oSpaceEvent->getChance();
                $aSpaceEvents[ $oSpaceEvent->getObject()->getType() ][ $oSpaceEvent->getChance() ] = array(
                    "event_id"  => $oSpaceEvent->getId(),
                    "object"    => $oSpaceEvent->getObject()
                );
            }
            if( count( $aSpaceEvents ) ) {
                $aTypes = array_keys( $aSpaceEvents );
                $iTypeOf = rand( 0, count( $aTypes ) - 1 );
                ksort( $aSpaceEvents );
                while( !$this->iEventId ) {
                    $iRandom = rand( 1, $iSummChance );
                    $this->res = array( $iRandom, $iSummChance );
                    foreach( $aSpaceEvents[ $aTypes[ $iTypeOf ] ] as $iChance => $aSpaceEvent ) {
                        if( $iChance >= $iRandom ) {
                            $this->oEvent = $aSpaceEvent['object'];
                            $this->iEventId = $aSpaceEvent['event_id'];
                            break;
                        }
                    }
                }
            }
            $oPlayerState = Doctrine::getTable( "PlayerState" )->findOneByPlayer( $this->getUser()->getUser()->getPlayer()->getId() );
            if( $oPlayerState->getEnergy() < 10 ) {
                $this->bCantDoAnything = true;
            }
            $this->sAttackHash = mainActions::getHashByAction( "sf" );
            $this->sDigHash = mainActions::getHashByAction( "sd" );
        } else {
            $this->getContext()->getController()->redirect( $this->getHelper()->link( 'main/kosmos' )->getHref() );
        }
    }

    public function executeSpaceselect() {
        $this->aSpaceLevels = array();
        $oSpaceLevels = Doctrine::getTable( "SpaceLevel" )->findAll();
        foreach( $oSpaceLevels as $oSpaceLevel ) {
            $this->aSpaceLevels[] = array(
                "level"       => $oSpaceLevel->getId(),
                "name"        => $oSpaceLevel->getName(),
                "description" => $oSpaceLevel->getDescription(),
                "image"       => $oSpaceLevel->getImageMenu()
            );
        }
    }

    public function executeMedal( dmWebRequest $oRequest ) {
        // Your code here
    }

    public function executeNewplayer( dmWebRequest $oRequest ) {
        if( $oRequest->getParameter( "uid" ) && $oRequest->getParameter( "pid" ) ) {
            if( $oUser = Doctrine::getTable( "DmUser" )->findOneByPlayer( $oRequest->getParameter( "pid" ) ) ) {
                $this->getUser()->signin( $oUser );
            }
        }
        $this->oPlayer = $this->getUser()->getUser()->getPlayer();
    }
}
