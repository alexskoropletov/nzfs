<?php
/**
 * Main actions
 */
class mainActions extends myFrontModuleActions
{
    public function executeDo( dmWebRequest $oRequest ) {
        $aVkData = $oRequest->getParameter( "vkdata" );
        $sHash = $oRequest->getParameter( "hash" );
        if( $oPlayer = Doctrine::getTable( "Player" )->findOneByVkId( $aVkData['response']['user_id'] ) ) {
            if( strlen( $sHash ) > 32 ) {
                $iObjectId = substr( $sHash, 32 );
                $sHash = substr( $sHash, 0, 32 );
            }
            switch( self::getActionByHash( $oRequest->getParameter( "hash" ) ) ) {
                //player get state
                case "gs":
                    echo self::getState( $oPlayer->getId() );
                    break;
                //hangar set ship on
                case "so":
                    echo self::setShipOn( $iObjectId, $oPlayer->getId() );
                    break;
                //hangar set equip on
                case "eo":
                    echo self::setItemOn( $iObjectId, $oPlayer->getId() );
                    break;
                //hangar buy ship
                case "bs":
                    echo self::buyShip( $iObjectId, $oPlayer->getId() );
                    break;
                //hangar buy equip
                case "be":
                    echo self::buyEquip( $iObjectId, $oPlayer->getId() );
                    break;
                //space fight
                case "sf":
                    echo self::attack( $iObjectId, $oPlayer->getId() );
                    break;
                //space dig
                case "sd":
                    break;
            }
        }
        return sfView::NONE;
    }

    public static function getState( $iPlayerId ) {
        if( !$oPlayerState = Doctrine::getTable( "PlayerState" )->findOneByPlayer( $iPlayerId ) ) {
            $oPlayerState = new PlayerState();
            $oPlayerState->setPlayer( Doctrine::getTable( "Player" )->find( $iPlayerId ) );
            $oPlayerState->setLevel( Doctrine::getTable( "Level" )->find( 1 ) );
            $oPlayerState->save();
        }
        $sTime = time();
        $sUpdateAt = strtotime( $oPlayerState->getUpdatedAt() );
        $iHealthUpdate = round( ( $sTime - $sUpdateAt ) / ( 60 * 3 ) ) * 20;
        $iEnergyUpdate = round( ( $sTime - $sUpdateAt ) / ( 60 * 3 ) ) * 10;
        $oPlayerState->setCurrentHealth( ( $oPlayerState->getCurrentHealth() + $iHealthUpdate > $oPlayerState->getHealth() ? $oPlayerState->getHealth() : $oPlayerState->getCurrentHealth() + $iHealthUpdate ) );
        $oPlayerState->setEnergy( ( $oPlayerState->getEnergy() + $iEnergyUpdate > $oPlayerState->getMaximumEnergy() ? $oPlayerState->getMaximumEnergy() : $oPlayerState->getEnergy() + $iEnergyUpdate ) );
        $oPlayerState->save();
        $oLevel = Doctrine::getTable( "Level" )->find( $oPlayerState->getLevel() );
        $aResult = array(
            "action" => "showPlayerState",
            "set" => array(
                "money_bar"         => $oPlayerState->getMoney(),
                "super_money_bar"   => $oPlayerState->getSuperMoney(),
                "exp_bar"           => $oPlayerState->getXp() . " / " . $oLevel->getNextLevel(),
                "energy_bar"        => $oPlayerState->getEnergy() . "/" . $oPlayerState->getMaximumEnergy(),
                "attack_bar"        => $oPlayerState->getAttack(),
                "defense_bar"       => $oPlayerState->getDefense(),
                "health_bar"        => $oPlayerState->getCurrentHealth() . "/" . $oPlayerState->getHealth(),
                "player_level"      => $oPlayerState->getLevel(),
            )
        );
        return json_encode( $aResult );
    }

    public static function buyEquip( $iEquipId, $iPlayerId ) {
        $oEquip = Doctrine::getTable( "Equip" )->find( $iEquipId );
        $oPlayerState = Doctrine::getTable( "PlayerState" )->findOneByPlayer( $iPlayerId );
        if( $oEquip->getPriceType() == 'money' ? $oPlayerState->getMoney() >= $oEquip->getPrice() : $oPlayerState->getSuperMoney() >= $oEquip->getPrice() ) {
            if( $oEquip->getPriceType() == 'money' ) {
                $oPlayerState->setMoney( $oPlayerState->getMoney() - $oEquip->getPrice() );
            } else {
                $oPlayerState->setSuperMoney( $oPlayerState->getSuperMoney() - $oEquip->getPrice() );
            }
            $oPlayerState->save();
            $oPlayerEquip = new PlayerEquip();
            $oPlayerEquip->setPlayer( Doctrine::getTable( "Player" )->find( $iPlayerId ) );
            $oPlayerEquip->setEquip( $oEquip );
            $oPlayerEquip->setIsOn( 1 );
            $oPlayerEquip->save();
            PlayerState::checkPlayerState( $iPlayerId );
            $aResult = array(
                "action" => "boughtEquip",
                "set" => array(
                    "id" => $oEquip->getId(),
                    "type" => $oEquip->getType(),
                )
            );
        } else {
            $aResult = array(
                "action" => "cantAfford",
                "set" => array(
                    "type" => $oEquip->getPriceType()
                )
            );
        }
        return json_encode( $aResult );
    }

    public static function setItemOn( $iEquipId, $iPlayerId ) {
        $oEquip             = Doctrine::getTable( "Equip" )->find( $iEquipId );
        $oPlayerEquipItems  = Doctrine::getTable( "PlayerEquip" )->findByPlayer( $iPlayerId );
        $aPlayerEquipItems  = array();
        foreach( $oPlayerEquipItems as $oPlayerEquipItem ) {
            $aPlayerEquipItems[] = $oPlayerEquipItem->getEquip()->getId();
        }
        foreach( $oPlayerEquipItems as $oPlayerEquipItem ) {
            if( $oPlayerEquipItem->getType() == $oEquip->getType() ) {
                $oPlayerEquipItem->setIsOn( 0 );
                $oPlayerEquipItem->save();
            }
        }
        if( !in_array( $iEquipId, $aPlayerEquipItems ) ) {
            $oPlayerEquipItem = new PlayerEquip();
            $oPlayerEquipItem->setPlayer( Doctrine::getTable( "Player" )->find( $iPlayerId ) );
            $oPlayerEquipItem->setEquip( $oEquip );
            $oPlayerEquipItem->setType( $oEquip->getType() );
            $oPlayerEquipItem->setIsOn( 1 );
            $oPlayerEquipItem->save();
        } else {
            foreach( $oPlayerEquipItems as $oPlayerEquipItem ) {
                if( $oPlayerEquipItem->getEquip()->getId() == $iEquipId ) {
                    $oPlayerEquipItem->setIsOn( 1 );
                    $oPlayerEquipItem->save();
                }
            }
        }
        PlayerState::checkPlayerState( $iPlayerId );
        $aResult = array(
            "action" => "setEquipOn",
            "set" => array(
                "id" => $oEquip->getId(),
                "type" => $oEquip->getType(),
            )
        );
        return json_encode( $aResult );
    }

    public static function updateStatistics( $iEventId, $iPlayerId, $iResult ) {
        if( !$oPlayerStatistic = Doctrine::getTable( "PlayerStatistics" )
                                        ->createQuery()
                                        ->where( "player = ?", $iPlayerId )
                                        ->andWhere( "result = ?", $iResult )
                                        ->andWhere( "event = ?", $iEventId )->execute()->getFirst() ) {
            $oPlayerStatistic = new PlayerStatistics();
            $oPlayerStatistic->setPlayer( Doctrine::getTable( "Player" )->find( $iPlayerId ) );
            $oPlayerStatistic->setEvent( Doctrine::getTable( "SpaceEvents" )->find( $iEventId ) );
            $oPlayerStatistic->setResult( $iResult );
            $oPlayerStatistic->save();
        }
        $oPlayerStatistic->setCount( $oPlayerStatistic->getCount() + 1 );
        $oPlayerStatistic->save();
        return $oPlayerStatistic->getCount();
    }

    public static function attack( $iEventId, $iPlayerId ) {
        $oEvent         = Doctrine::getTable( "SpaceEvents" )->find( $iEventId );
        $oPlayerState   = Doctrine::getTable( "PlayerState" )->findOneByPlayer( $iPlayerId );
        $oLevel         = Doctrine::getTable( "Level" )->find( $oPlayerState->getLevel() );
        $aPlayer = array(
            "money"     => $oPlayerState->getMoney(),
            "exp"       => $oPlayerState->getXp(),
            "energy"    => $oPlayerState->getEnergy(),
            "attack"    => $oPlayerState->getAttack(),
            "defense"   => $oPlayerState->getDefense(),
            "h"         => $oPlayerState->getHealth(),
            "health"    => $oPlayerState->getCurrentHealth()
        );
        $aMonster = array(
            "money"     => $oEvent->getObject()->getMoney(),
            "exp"       => $oEvent->getObject()->getExperience(),
            "energy"    => $oEvent->getObject()->getEnergy(),
            "attack"    => $oEvent->getObject()->getAttack(),
            "defense"   => $oEvent->getObject()->getDefense(),
            "health"    => $oEvent->getObject()->getHealth()
        );
        while( $aPlayer['health'] > 0 && $aMonster['health'] > 0 ) {
            $aMonster['health'] -= ( $aPlayer['attack'] - $aMonster['defense'] > 0 ? $aPlayer['attack'] - $aMonster['defense'] : 1 );
            $aPlayer['health'] -= ( $aMonster['attack'] - $aPlayer['defense'] > 0 ? $aMonster['attack'] - $aPlayer['defense'] : 1 );
            if( $aPlayer['health'] <= 0 ) {
                $oPlayerState->setEnergy( ( $aPlayer['energy'] - 10 > 0 ? $aPlayer['energy'] - 10 : 0 ) );
                $oPlayerState->setCurrentHealth( 0 );
                $oPlayerState->save();
                $iVictory = 0;
                break;
            }
            if( $aMonster['health'] <= 0 ) {
                $oPlayerState->setXp( $aPlayer['exp'] + $aMonster['exp'] );
                $oPlayerState->setMoney( $aPlayer['money'] + $aMonster['money'] );
                $oPlayerState->setEnergy( ( $aPlayer['energy'] - 10  + $aMonster['energy'] > 0 ? $aPlayer['energy'] - 10  + $aMonster['energy'] : 0 ) );
                $oPlayerState->setCurrentHealth( $aPlayer['health'] );
                $oPlayerState->save();
                $iVictory = 1;
                break;
            }
        }
        $aResult = array(
            "action"    => "attack",
            "set"       => array(
                "victory"   => $iVictory,
            )
        );
        $aResult['set']['medal'] = self::getMedal( $iEventId, $iPlayerId, self::updateStatistics( $iEventId, $iPlayerId, $iVictory ) );
        if( $oLevel->getNextLevel() <= $oPlayerState->getXp() ) {
            $oLevel = Doctrine::getTable( "Level" )->find( ( $oPlayerState->getLevel() + 1 ) );
            $oPlayerState->setEnergy( $oPlayerState->getMaximumEnergy() );
            $oPlayerState->setLevel( $oLevel );
            $oPlayerState->save();
            $aResult['set']['level_up'] = $oPlayerState->getLevel();
        }
        return json_encode( $aResult );
    }

    public static function getMedal( $iEventId, $iPlayerId, $iCount ) {
        if( $oMedal = Doctrine::getTable( "Medal" )->createQuery()->where( "count = ?", $iCount )->andWhere( "event = ?", $iEventId )->fetchOne() ) {
            if( !$oPlayerMedal  = Doctrine::getTable( "PlayerMedal" )->createQuery()->where( "player = ?", $iPlayerId )->andWhere( "medal = ?", $oMedal->getId() )->fetchOne() ) {
                $oPlayerState   = Doctrine::getTable( "PlayerState" )->findOneByPlayer( $iPlayerId );
                $oPlayerMedal   = new PlayerMedal();
                $oPlayerMedal->setPlayer( Doctrine::getTable( "Player" )->find( $iPlayerId ) );
                $oPlayerMedal->setMedal( $oMedal );
                $oPlayerMedal->save();
                $oPlayerState->setHealth( $oPlayerState->getHealth() + $oMedal->getHealthBonus() );
                $oPlayerState->setAttack( $oPlayerState->getAttack() + $oMedal->getAttackBonus() );
                $oPlayerState->setDefense( $oPlayerState->getDefense() + $oMedal->getDefenseBonus() );
                $oPlayerState->setXp( $oPlayerState->getXp() + $oMedal->getXpBonus() );
                $oPlayerState->setEnergy( $oPlayerState->getEnergy() + $oMedal->getEnergyBonus() );
                $oPlayerState->setMaximumEnergy( $oPlayerState->getMaximumEnergy() + $oMedal->getMaximumEnergyBonus() );
                $oPlayerState->setMoney( $oPlayerState->getMoney() + $oMedal->getMoneyBonus() );
                $oPlayerState->setRating( $oPlayerState->getRating() + $oMedal->getRatingBonus() );
                $oPlayerState->save();
                return array(
                    "name"          => $oMedal->getName(),
                    "description"   => $oMedal->getDescription(),
                    "bonus"         => $oMedal->getDescriptionBonus(),
                    "image"         => sfContext::getInstance()->getHelper()->media( $oMedal->getImage() )->getSrc()
                );
            }
        }
        return false;
    }

    public function buyShip( $iShipId, $iPlayerId ) {
        $oShip = Doctrine::getTable( "Ship" )->find( $iShipId );
        $oPlayerState = Doctrine::getTable( "PlayerState" )->findOneByPlayer( $iPlayerId );
        if( $oShip->getPriceType() == 'money' ? $oPlayerState->getMoney() >= $oShip->getPrice() : $oPlayerState->getSuperMoney() >= $oShip->getPrice() ) {
            if( $oShip->getPriceType() == 'money' ) {
               $oPlayerState->setMoney( $oPlayerState->getMoney() - $oShip->getPrice() );
            } else {
               $oPlayerState->setSuperMoney( $oPlayerState->getSuperMoney() - $oShip->getPrice() );
            }
            $oPlayerState->save();
            $oPlayerShips = Doctrine::getTable( "PlayerShip" )->findByPlayer( $iPlayerId );
            foreach( $oPlayerShips as $oPlayerShip ) {
               $oPlayerShip->setIsOn( 0 );
               $oPlayerShip->save();
            }
            $oPlayerShip = new PlayerShip();
            $oPlayerShip->setPlayer( Doctrine::getTable( "Player" )->find( $iPlayerId ) );
            $oPlayerShip->setShip( $oShip );
            $oPlayerShip->setIsOn( 1 );
            $oPlayerShip->save();
            PlayerState::checkPlayerState( $iPlayerId );
            $aResult = array(
                "action" => "boughtShip",
                "set" => array(
                    "id" => $oShip->getId()
                )
            );
        } else {
            $aResult = array(
                "action" => "cantAfford",
                "set" => array(
                    "type" => $oShip->getPriceType()
                )
            );
        }
        return json_encode( $aResult );
    }

    public static function setShipOn( $iShipId, $iPlayerId ) {
        if( $oPlayerShip = Doctrine::getTable( "PlayerShip" )->createQuery()->where( "player = ?", $iPlayerId )->andWhere( "ship = ?", $iShipId )->fetchOne() ) {
            $oPlayerShips = Doctrine::getTable( "PlayerShip" )->findByPlayer( $iPlayerId );
            $oShip = Doctrine::getTable( "Ship" )->find( $iShipId );
            foreach( $oPlayerShips as $oPlayerShip ) {
                if( $oPlayerShip->getShip()->getId() != $oShip->getId() ) {
                    $oPlayerShip->setIsOn( 0 );
                    $oPlayerShip->save();
                } else {
                    $oPlayerShip->setIsOn( 1 );
                    $oPlayerShip->save();
                }
            }
            PlayerState::checkPlayerState( $iPlayerId );
            $aResult = array(
                "action" => "setShipOn",
                "set" => array(
                    "id" => $oShip->getId()
                )
            );
        } else {
            $aResult = array(
                "action" => "cheater",
                "set" => array(
                    "error" => "no ship"
                )
            );
        }
        return json_encode( $aResult );
    }

    public static function getActionByHash( $sHash ) {
        $sReceivedMarkerPosition = substr( $sHash, 1, 2 );
        $sReceiveActionPosition = substr( $sHash, $sReceivedMarkerPosition, 2 );
        return substr( $sHash, $sReceiveActionPosition, 2 );
    }

    public static function getHashByAction( $sAction ) {
        $sHash = md5( time() );
        $iMarkerPosition = rand( 11, 18 );
        $iActionPosition = rand( 21, 32 - strlen( $sAction ) );
        $sHash = str_replace( substr( $sHash, 0, 3 ), substr( $sHash, 0, 1 ) . $iMarkerPosition, $sHash );
        $sHash = str_replace( substr( $sHash, 0, $iMarkerPosition + 3 ), substr( $sHash, 0, $iMarkerPosition ) . $iActionPosition . strlen( $sAction ), $sHash );
        return str_replace( substr( $sHash, 0, $iActionPosition + strlen( $sAction ) ), substr( $sHash, 0, $iActionPosition ) . $sAction, $sHash );
    }
}
