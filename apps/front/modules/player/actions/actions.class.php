<?php
/**
 * Player actions
 * 
 */
class playerActions extends myFrontModuleActions {
    public function executeGetPlayer(dmWebRequest $oRequest) {
        $aUserData = $oRequest->getParameter( "userdata" );
        if( !$this->getUser()->isAuthenticated() ) {
            if( isset( $aUserData['user_id'] ) ) {
                if( $oPlayer = Doctrine::getTable( "Player" )->findOneByVkId( $aUserData['user_id'] ) ) {
                    echo $this->getHelper()->link( "main/angar" )->param( "pid", $oPlayer->getId() )->param( "uid", $aUserData['user_id'] )->getHref();
                } else {
                    $oPlayer = new Player();
                    $oPlayer->setVkId( $aUserData['user_id'] );
                    $oPlayer->setName( $aUserData['user_name'] );
                    $oPlayer->setGender( $aUserData['user_sex'] == 2 ? "male" : "female" );
                    $oPlayer->setImageVk( $aUserData['user_photo'] );
                    $oPlayer->save();
                    $oUser = new DmUser();
                    $oUser->setUsername( "id" . $aUserData['user_id'] );
                    $oUser->setIsActive( true );
                    $oUser->setPlayer( $oPlayer );
                    $oUser->setEmail( "id" . $aUserData['user_id'] . "@somemail.ru" );
                    $oUser->save();
                    $oShip = Doctrine::getTable( "Ship" )->find( 1 );
                    $oPlayerShip = new PlayerShip();
                    $oPlayerShip->setPlayer( $oPlayer );
                    $oPlayerShip->setShip( $oShip );
                    $oPlayerShip->setIsOn( true );
                    $oPlayerShip->save();
                    $oPlayerState = new PlayerState();
                    $oPlayerState->setPlayer( $oPlayer );
                    $oPlayerState->setLevel( Doctrine::getTable( "Level" )->find( 1 ) );
                    $oPlayerState->setAttack( $oShip->getAttack() );
                    $oPlayerState->setDefense( $oShip->getDefense() );
                    $oPlayerState->setEnergy( 100 );
                    $oPlayerState->setHealth( $oShip->getHealth() );
                    $oPlayerState->setCurrentHealth( $oShip->getHealth() );
                    $oPlayerState->save();
                    echo $this->getHelper()->link( 'main/novyiyIgrok' )->param( "pid", $oPlayer->getId() )->param( "uid", $aUserData['user_id'] )->getHref();
                }
            }
        } else {
            echo $this->getHelper()->link( "main/angar" )->getHref();
        }
        return sfView::NONE;
    }



    public function executeSearch(dmWebRequest $oRequest) {
        $iPlayerId      = $oRequest->getParameter( "iPlayerId" );
        $iEvent         = $oRequest->getParameter( "iEventId" );
        $oEquip         = Doctrine::getTable( "SpaceObject" )->find( $oRequest->getParameter( "iEquipId" ) );
        $oPlayerState   = Doctrine::getTable( "PlayerState" )->findOneByPlayer( $iPlayerId );
        $oLevel         = Doctrine::getTable( "Level" )->find( $oPlayerState->getLevel() );
        $oPlayerState->setEnergy( ( $oPlayerState->getEnergy() - 10 + $oEquip->getEnergy() > 0 ? $oPlayerState->getEnergy() - 10 + $oEquip->getEnergy() : 0 ) );
        $oPlayerState->setMoney( $oPlayerState->getMoney() + $oEquip->getMoney() );
        $oPlayerState->setXp( $oPlayerState->getXp() + $oEquip->getExperience() );
        $oPlayerState->save();
        if( !$oPlayerWinStatistic = Doctrine::getTable( "PlayerStatistics" )->createQuery( "a" )->where( "a.player = ?", $iPlayerId )->andWhere( "a.event = ?", $iEvent )->execute()->getFirst() ) {
            $oPlayerWinStatistic = new PlayerStatistics();
            $oPlayerWinStatistic->setPlayer( Doctrine::getTable( "Player" )->find( $iPlayerId ) );
            $oPlayerWinStatistic->setEvent( Doctrine::getTable( "SpaceEvents" )->find( $iEvent ) );
        }
        $oPlayerWinStatistic->setCount( $oPlayerWinStatistic->getCount() + 1 );
        $oPlayerWinStatistic->save();
        $aResult = array(
            "money"     => $oPlayerState->getMoney(),
            "exp"       => $oPlayerState->getXp() . " / " . $oLevel->getNextLevel(),
            "energy"    => $oPlayerState->getEnergy() . "/" . $oPlayerState->getMaximumEnergy(),
            "attack"    => $oPlayerState->getAttack(),
            "defense"   => $oPlayerState->getDefense(),
            "health"    => $oPlayerState->getCurrentHealth() . "/" . $oPlayerState->getHealth(),
            "level"     => $oPlayerState->getLevel()
        );
        if( $oMedal = Doctrine::getTable( "Medal" )->createQuery()->where( "count = ?", $oPlayerWinStatistic->getCount() )->andWhere( "event = ?", $iEvent )->fetchOne() ) {
            if( !$oPlayerMedal = Doctrine::getTable( "PlayerMedal" )->createQuery()->where( "player = ?", $iPlayerId )->andWhere( "medal = ?", $oMedal->getId() ) ) {
                $oPlayerMedal = new PlayerMedal();
                $oPlayerMedal->setPlayer( Doctrine::getTable( "Player" )->find( $iPlayerId ) );
                $oPlayerMedal->setMedal( $oMedal );
                $oPlayerMedal->save();
                $oPlayerState->setHealth( $oPlayerState->getHealth() + $oMedal->getHealthBonus() );
                $oPlayerState->setAttack( $oPlayerState->getAttack() + $oMedal->getAttackBonus() );
                $oPlayerState->setDefense( $oPlayerState->getDefense() + $oMedal->getDefenseBonus() );
                $oPlayerState->setXp( $oPlayerState->getXp() + $oMedal->getXpBonus() );
                $oPlayerState->setEnergy( $oPlayerState->getEnergy() + $oMedal->getEnergyBonus() );
                $oPlayerState->setMaximumEnergy( $oPlayerState->getMaximumEnergy() + $oMedal->getMaximumEnergyBonus() );
                $oPlayerState->setMoney( $oPlayerState->getMoney() + $oMedal->getMoneyBonus() );
                $oPlayerState->setRating( $oPlayerState->getRating() + $oMedal->getRatingBonus() );
                $oPlayerState->save();
                $aResult['medal'] = array(
                    "name" => $oMedal->getName(),
                    "description" => $oMedal->getDescription(),
                    "bonus" => $oMedal->getDescriptionBonus(),
                    "image" => $this->getHelper()->media( $oMedal->getImage()->getSrc() )
                );
            }
        }
        if( $oLevel->getNextLevel() <= $oPlayerState->getXp() ) {
            $oLevel = Doctrine::getTable( "Level" )->find( ( $oPlayerState->getLevel() + 1 ) );
            $oPlayerState->setEnergy( $oPlayerState->getMaximumEnergy() );
            $oPlayerState->setLevel( $oLevel );
            $oPlayerState->save();
            $aResult['level_up'] = $oPlayerState->getLevel();
            $aResult['level'] = $oPlayerState->getLevel();
            $aResult['exp'] = $oPlayerState->getXp() . " / " . $oLevel->getNextLevel();
            $aResult["energy"] = $oPlayerState->getMaximumEnergy() . "/" . $oPlayerState->getMaximumEnergy();
        }
        echo json_encode( $aResult );
        return sfView::NONE;
    }

    public function executeFormWidget(dmWebRequest $request) {
        $form = new PlayerForm();
        
        if ($request->hasParameter($form->getName()) && $form->bindAndValid($request))
        {
          $form->save();
          $this->redirectBack();
        }

        $this->forms['Player'] = $form;
    }
}
