<?php

require_once dirname(__FILE__).'/../lib/spaceObjectGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/spaceObjectGeneratorHelper.class.php';

/**
 * spaceObject actions.
 *
 * @package    nzfs
 * @subpackage spaceObject
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class spaceObjectActions extends autoSpaceObjectActions
{
}
