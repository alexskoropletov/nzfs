<?php


class SpaceEventsTable extends myDoctrineTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('SpaceEvents');
    }
}