<?php


class PlayerStatisticsTable extends myDoctrineTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('PlayerStatistics');
    }
}