<?php


class SpaceObjectTable extends myDoctrineTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('SpaceObject');
    }
}