<?php

/**
 * SpaceLevel form base class.
 *
 * @method SpaceLevel getObject() Returns the current form's model object
 *
 * @package    nzfs
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id$
 */
abstract class BaseSpaceLevelForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'          => new sfWidgetFormInputHidden(),
      'name'        => new sfWidgetFormInputText(),
      'description' => new sfWidgetFormInputText(),
      'image_menu'  => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('ImageMenu'), 'add_empty' => true)),

    ));

    $this->setValidators(array(
      'id'          => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'name'        => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'description' => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'image_menu'  => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('ImageMenu'), 'required' => false)),
    ));

    /*
     * Embed Media form for image_menu
     */
    $this->embedForm('image_menu_form', $this->createMediaFormForImageMenu());
    unset($this['image_menu']);

    $this->widgetSchema->setNameFormat('space_level[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
    
    // Unset automatic fields like 'created_at', 'updated_at', 'position'
    // override this method in your form to keep them
    parent::unsetAutoFields();
  }

  /**
   * Creates a DmMediaForm instance for image_menu
   *
   * @return DmMediaForm a form instance for the related media
   */
  protected function createMediaFormForImageMenu()
  {
    return DmMediaForRecordForm::factory($this->object, 'image_menu', 'ImageMenu', $this->validatorSchema['image_menu']->getOption('required'));
  }

  protected function doBind(array $values)
  {
    $values = $this->filterValuesByEmbeddedMediaForm($values, 'image_menu');
    parent::doBind($values);
  }
  
  public function processValues($values)
  {
    $values = parent::processValues($values);
    $values = $this->processValuesForEmbeddedMediaForm($values, 'image_menu');
    return $values;
  }
  
  protected function doUpdateObject($values)
  {
    parent::doUpdateObject($values);
    $this->doUpdateObjectForEmbeddedMediaForm($values, 'image_menu', 'ImageMenu');
  }

  public function getModelName()
  {
    return 'SpaceLevel';
  }

}