<?php

/**
 * PlayerState form base class.
 *
 * @method PlayerState getObject() Returns the current form's model object
 *
 * @package    nzfs
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id$
 */
abstract class BasePlayerStateForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'             => new sfWidgetFormInputHidden(),
      'player'         => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Player'), 'add_empty' => true)),
      'health'         => new sfWidgetFormInputText(),
      'current_health' => new sfWidgetFormInputText(),
      'attack'         => new sfWidgetFormInputText(),
      'defense'        => new sfWidgetFormInputText(),
      'xp'             => new sfWidgetFormInputText(),
      'level'          => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Leveling'), 'add_empty' => true)),
      'energy'         => new sfWidgetFormInputText(),
      'maximum_energy' => new sfWidgetFormInputText(),
      'money'          => new sfWidgetFormInputText(),
      'super_money'    => new sfWidgetFormInputText(),
      'rating'         => new sfWidgetFormInputText(),
      'created_at'     => new sfWidgetFormDateTime(),
      'updated_at'     => new sfWidgetFormDateTime(),

    ));

    $this->setValidators(array(
      'id'             => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'player'         => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Player'), 'required' => false)),
      'health'         => new sfValidatorInteger(array('required' => false)),
      'current_health' => new sfValidatorInteger(array('required' => false)),
      'attack'         => new sfValidatorInteger(array('required' => false)),
      'defense'        => new sfValidatorInteger(array('required' => false)),
      'xp'             => new sfValidatorInteger(array('required' => false)),
      'level'          => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Leveling'), 'required' => false)),
      'energy'         => new sfValidatorInteger(array('required' => false)),
      'maximum_energy' => new sfValidatorInteger(array('required' => false)),
      'money'          => new sfValidatorInteger(array('required' => false)),
      'super_money'    => new sfValidatorInteger(array('required' => false)),
      'rating'         => new sfValidatorInteger(array('required' => false)),
      'created_at'     => new sfValidatorDateTime(),
      'updated_at'     => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('player_state[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
    
    // Unset automatic fields like 'created_at', 'updated_at', 'position'
    // override this method in your form to keep them
    parent::unsetAutoFields();
  }


  protected function doBind(array $values)
  {
    parent::doBind($values);
  }
  
  public function processValues($values)
  {
    $values = parent::processValues($values);
    return $values;
  }
  
  protected function doUpdateObject($values)
  {
    parent::doUpdateObject($values);
  }

  public function getModelName()
  {
    return 'PlayerState';
  }

}