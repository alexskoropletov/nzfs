<?php

/**
 * PlayerEquip form base class.
 *
 * @method PlayerEquip getObject() Returns the current form's model object
 *
 * @package    nzfs
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id$
 */
abstract class BasePlayerEquipForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'     => new sfWidgetFormInputHidden(),
      'player' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Player'), 'add_empty' => true)),
      'equip'  => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Equip'), 'add_empty' => true)),
      'type'   => new sfWidgetFormChoice(array('choices' => array('weapon' => 'weapon', 'health' => 'health', 'defense' => 'defense'))),
      'is_on'  => new sfWidgetFormInputCheckbox(),

    ));

    $this->setValidators(array(
      'id'     => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'player' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Player'), 'required' => false)),
      'equip'  => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Equip'), 'required' => false)),
      'type'   => new sfValidatorChoice(array('choices' => array(0 => 'weapon', 1 => 'health', 2 => 'defense'), 'required' => false)),
      'is_on'  => new sfValidatorBoolean(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('player_equip[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
    
    // Unset automatic fields like 'created_at', 'updated_at', 'position'
    // override this method in your form to keep them
    parent::unsetAutoFields();
  }


  protected function doBind(array $values)
  {
    parent::doBind($values);
  }
  
  public function processValues($values)
  {
    $values = parent::processValues($values);
    return $values;
  }
  
  protected function doUpdateObject($values)
  {
    parent::doUpdateObject($values);
  }

  public function getModelName()
  {
    return 'PlayerEquip';
  }

}