<?php

/**
 * SpaceEvents form base class.
 *
 * @method SpaceEvents getObject() Returns the current form's model object
 *
 * @package    nzfs
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id$
 */
abstract class BaseSpaceEventsForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'          => new sfWidgetFormInputHidden(),
      'space_level' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('SpaceLevel'), 'add_empty' => true)),
      'chance'      => new sfWidgetFormInputText(),
      'object'      => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Object'), 'add_empty' => true)),

    ));

    $this->setValidators(array(
      'id'          => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'space_level' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('SpaceLevel'), 'required' => false)),
      'chance'      => new sfValidatorInteger(array('required' => false)),
      'object'      => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Object'), 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('space_events[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
    
    // Unset automatic fields like 'created_at', 'updated_at', 'position'
    // override this method in your form to keep them
    parent::unsetAutoFields();
  }


  protected function doBind(array $values)
  {
    parent::doBind($values);
  }
  
  public function processValues($values)
  {
    $values = parent::processValues($values);
    return $values;
  }
  
  protected function doUpdateObject($values)
  {
    parent::doUpdateObject($values);
  }

  public function getModelName()
  {
    return 'SpaceEvents';
  }

}