<?php

/**
 * Medal form base class.
 *
 * @method Medal getObject() Returns the current form's model object
 *
 * @package    nzfs
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id$
 */
abstract class BaseMedalForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                   => new sfWidgetFormInputHidden(),
      'name'                 => new sfWidgetFormInputText(),
      'description'          => new sfWidgetFormInputText(),
      'description_bonus'    => new sfWidgetFormInputText(),
      'image'                => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Image'), 'add_empty' => true)),
      'event'                => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('SpaceEvent'), 'add_empty' => true)),
      'result'               => new sfWidgetFormInputCheckbox(),
      'count'                => new sfWidgetFormInputText(),
      'type'                 => new sfWidgetFormChoice(array('choices' => array('permanent' => 'permanent', 'onetime' => 'onetime'))),
      'health_bonus'         => new sfWidgetFormInputText(),
      'attack_bonus'         => new sfWidgetFormInputText(),
      'defense_bonus'        => new sfWidgetFormInputText(),
      'xp_bonus'             => new sfWidgetFormInputText(),
      'energy_bonus'         => new sfWidgetFormInputText(),
      'maximum_energy_bonus' => new sfWidgetFormInputText(),
      'money_bonus'          => new sfWidgetFormInputText(),
      'rating_bonus'         => new sfWidgetFormInputText(),

    ));

    $this->setValidators(array(
      'id'                   => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'name'                 => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'description'          => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'description_bonus'    => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'image'                => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Image'), 'required' => false)),
      'event'                => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('SpaceEvent'), 'required' => false)),
      'result'               => new sfValidatorBoolean(array('required' => false)),
      'count'                => new sfValidatorInteger(array('required' => false)),
      'type'                 => new sfValidatorChoice(array('choices' => array(0 => 'permanent', 1 => 'onetime'), 'required' => false)),
      'health_bonus'         => new sfValidatorInteger(array('required' => false)),
      'attack_bonus'         => new sfValidatorInteger(array('required' => false)),
      'defense_bonus'        => new sfValidatorInteger(array('required' => false)),
      'xp_bonus'             => new sfValidatorInteger(array('required' => false)),
      'energy_bonus'         => new sfValidatorInteger(array('required' => false)),
      'maximum_energy_bonus' => new sfValidatorInteger(array('required' => false)),
      'money_bonus'          => new sfValidatorInteger(array('required' => false)),
      'rating_bonus'         => new sfValidatorInteger(array('required' => false)),
    ));

    /*
     * Embed Media form for image
     */
    $this->embedForm('image_form', $this->createMediaFormForImage());
    unset($this['image']);

    $this->widgetSchema->setNameFormat('medal[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
    
    // Unset automatic fields like 'created_at', 'updated_at', 'position'
    // override this method in your form to keep them
    parent::unsetAutoFields();
  }

  /**
   * Creates a DmMediaForm instance for image
   *
   * @return DmMediaForm a form instance for the related media
   */
  protected function createMediaFormForImage()
  {
    return DmMediaForRecordForm::factory($this->object, 'image', 'Image', $this->validatorSchema['image']->getOption('required'));
  }

  protected function doBind(array $values)
  {
    $values = $this->filterValuesByEmbeddedMediaForm($values, 'image');
    parent::doBind($values);
  }
  
  public function processValues($values)
  {
    $values = parent::processValues($values);
    $values = $this->processValuesForEmbeddedMediaForm($values, 'image');
    return $values;
  }
  
  protected function doUpdateObject($values)
  {
    parent::doUpdateObject($values);
    $this->doUpdateObjectForEmbeddedMediaForm($values, 'image', 'Image');
  }

  public function getModelName()
  {
    return 'Medal';
  }

}