<?php

/**
 * Equip form base class.
 *
 * @method Equip getObject() Returns the current form's model object
 *
 * @package    nzfs
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id$
 */
abstract class BaseEquipForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'          => new sfWidgetFormInputHidden(),
      'name'        => new sfWidgetFormInputText(),
      'description' => new sfWidgetFormInputText(),
      'level'       => new sfWidgetFormInputText(),
      'price'       => new sfWidgetFormInputText(),
      'price_type'  => new sfWidgetFormChoice(array('choices' => array('money' => 'money', 'super_money' => 'super_money'))),
      'type'        => new sfWidgetFormChoice(array('choices' => array('weapon' => 'weapon', 'health' => 'health', 'defense' => 'defense'))),
      'modifier'    => new sfWidgetFormInputText(),
      'is_saleable' => new sfWidgetFormInputCheckbox(),
      'image'       => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Image'), 'add_empty' => true)),
      'image_small' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('ImageSmall'), 'add_empty' => true)),

    ));

    $this->setValidators(array(
      'id'          => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'name'        => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'description' => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'level'       => new sfValidatorInteger(array('required' => false)),
      'price'       => new sfValidatorInteger(array('required' => false)),
      'price_type'  => new sfValidatorChoice(array('choices' => array(0 => 'money', 1 => 'super_money'), 'required' => false)),
      'type'        => new sfValidatorChoice(array('choices' => array(0 => 'weapon', 1 => 'health', 2 => 'defense'), 'required' => false)),
      'modifier'    => new sfValidatorInteger(array('required' => false)),
      'is_saleable' => new sfValidatorBoolean(array('required' => false)),
      'image'       => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Image'), 'required' => false)),
      'image_small' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('ImageSmall'), 'required' => false)),
    ));

    /*
     * Embed Media form for image
     */
    $this->embedForm('image_form', $this->createMediaFormForImage());
    unset($this['image']);

    /*
     * Embed Media form for image_small
     */
    $this->embedForm('image_small_form', $this->createMediaFormForImageSmall());
    unset($this['image_small']);

    $this->widgetSchema->setNameFormat('equip[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
    
    // Unset automatic fields like 'created_at', 'updated_at', 'position'
    // override this method in your form to keep them
    parent::unsetAutoFields();
  }

  /**
   * Creates a DmMediaForm instance for image
   *
   * @return DmMediaForm a form instance for the related media
   */
  protected function createMediaFormForImage()
  {
    return DmMediaForRecordForm::factory($this->object, 'image', 'Image', $this->validatorSchema['image']->getOption('required'));
  }
  /**
   * Creates a DmMediaForm instance for image_small
   *
   * @return DmMediaForm a form instance for the related media
   */
  protected function createMediaFormForImageSmall()
  {
    return DmMediaForRecordForm::factory($this->object, 'image_small', 'ImageSmall', $this->validatorSchema['image_small']->getOption('required'));
  }

  protected function doBind(array $values)
  {
    $values = $this->filterValuesByEmbeddedMediaForm($values, 'image');
    $values = $this->filterValuesByEmbeddedMediaForm($values, 'image_small');
    parent::doBind($values);
  }
  
  public function processValues($values)
  {
    $values = parent::processValues($values);
    $values = $this->processValuesForEmbeddedMediaForm($values, 'image');
    $values = $this->processValuesForEmbeddedMediaForm($values, 'image_small');
    return $values;
  }
  
  protected function doUpdateObject($values)
  {
    parent::doUpdateObject($values);
    $this->doUpdateObjectForEmbeddedMediaForm($values, 'image', 'Image');
    $this->doUpdateObjectForEmbeddedMediaForm($values, 'image_small', 'ImageSmall');
  }

  public function getModelName()
  {
    return 'Equip';
  }

}