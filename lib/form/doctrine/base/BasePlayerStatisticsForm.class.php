<?php

/**
 * PlayerStatistics form base class.
 *
 * @method PlayerStatistics getObject() Returns the current form's model object
 *
 * @package    nzfs
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id$
 */
abstract class BasePlayerStatisticsForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'     => new sfWidgetFormInputHidden(),
      'player' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Player'), 'add_empty' => true)),
      'event'  => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('SpaceEvent'), 'add_empty' => true)),
      'result' => new sfWidgetFormInputCheckbox(),
      'count'  => new sfWidgetFormInputText(),

    ));

    $this->setValidators(array(
      'id'     => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'player' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Player'), 'required' => false)),
      'event'  => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('SpaceEvent'), 'required' => false)),
      'result' => new sfValidatorBoolean(array('required' => false)),
      'count'  => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('player_statistics[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
    
    // Unset automatic fields like 'created_at', 'updated_at', 'position'
    // override this method in your form to keep them
    parent::unsetAutoFields();
  }


  protected function doBind(array $values)
  {
    parent::doBind($values);
  }
  
  public function processValues($values)
  {
    $values = parent::processValues($values);
    return $values;
  }
  
  protected function doUpdateObject($values)
  {
    parent::doUpdateObject($values);
  }

  public function getModelName()
  {
    return 'PlayerStatistics';
  }

}