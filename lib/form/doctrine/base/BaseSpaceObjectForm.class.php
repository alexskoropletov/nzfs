<?php

/**
 * SpaceObject form base class.
 *
 * @method SpaceObject getObject() Returns the current form's model object
 *
 * @package    nzfs
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id$
 */
abstract class BaseSpaceObjectForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'          => new sfWidgetFormInputHidden(),
      'name'        => new sfWidgetFormInputText(),
      'description' => new sfWidgetFormTextarea(),
      'type'        => new sfWidgetFormChoice(array('choices' => array('enemy' => 'enemy', 'enemy_boss' => 'enemy_boss', 'excavation' => 'excavation', 'crash' => 'crash'))),
      'attack'      => new sfWidgetFormInputText(),
      'health'      => new sfWidgetFormInputText(),
      'defense'     => new sfWidgetFormInputText(),
      'image'       => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Image'), 'add_empty' => true)),
      'experience'  => new sfWidgetFormInputText(),
      'money'       => new sfWidgetFormInputText(),
      'energy'      => new sfWidgetFormInputText(),

    ));

    $this->setValidators(array(
      'id'          => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'name'        => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'description' => new sfValidatorString(array('required' => false)),
      'type'        => new sfValidatorChoice(array('choices' => array(0 => 'enemy', 1 => 'enemy_boss', 2 => 'excavation', 3 => 'crash'), 'required' => false)),
      'attack'      => new sfValidatorInteger(array('required' => false)),
      'health'      => new sfValidatorInteger(array('required' => false)),
      'defense'     => new sfValidatorInteger(array('required' => false)),
      'image'       => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Image'), 'required' => false)),
      'experience'  => new sfValidatorInteger(array('required' => false)),
      'money'       => new sfValidatorInteger(array('required' => false)),
      'energy'      => new sfValidatorInteger(array('required' => false)),
    ));

    /*
     * Embed Media form for image
     */
    $this->embedForm('image_form', $this->createMediaFormForImage());
    unset($this['image']);

    $this->widgetSchema->setNameFormat('space_object[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
    
    // Unset automatic fields like 'created_at', 'updated_at', 'position'
    // override this method in your form to keep them
    parent::unsetAutoFields();
  }

  /**
   * Creates a DmMediaForm instance for image
   *
   * @return DmMediaForm a form instance for the related media
   */
  protected function createMediaFormForImage()
  {
    return DmMediaForRecordForm::factory($this->object, 'image', 'Image', $this->validatorSchema['image']->getOption('required'));
  }

  protected function doBind(array $values)
  {
    $values = $this->filterValuesByEmbeddedMediaForm($values, 'image');
    parent::doBind($values);
  }
  
  public function processValues($values)
  {
    $values = parent::processValues($values);
    $values = $this->processValuesForEmbeddedMediaForm($values, 'image');
    return $values;
  }
  
  protected function doUpdateObject($values)
  {
    parent::doUpdateObject($values);
    $this->doUpdateObjectForEmbeddedMediaForm($values, 'image', 'Image');
  }

  public function getModelName()
  {
    return 'SpaceObject';
  }

}