<?php

/**
 * PlayerShip filter form base class.
 *
 * @package    nzfs
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BasePlayerShipFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'player' => new sfWidgetFormDoctrineChoice(array('model' => 'Player', 'add_empty' => true)),
      'ship'   => new sfWidgetFormDoctrineChoice(array('model' => 'Ship', 'add_empty' => true)),
      'is_on'  => new sfWidgetFormChoice(array('choices' => array('' => $this->getI18n()->__('yes or no', array(), 'dm'), 1 => $this->getI18n()->__('yes', array(), 'dm'), 0 => $this->getI18n()->__('no', array(), 'dm')))),
    ));

    $this->setValidators(array(
      'player' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Player'), 'column' => 'id')),
      'ship'   => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Ship'), 'column' => 'id')),
      'is_on'  => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
    ));
    

    $this->widgetSchema->setNameFormat('player_ship_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'PlayerShip';
  }

  public function getFields()
  {
    return array(
      'id'     => 'Number',
      'player' => 'ForeignKey',
      'ship'   => 'ForeignKey',
      'is_on'  => 'Boolean',
    );
  }
}
