<?php

/**
 * PlayerMedal filter form base class.
 *
 * @package    nzfs
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BasePlayerMedalFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'player' => new sfWidgetFormDoctrineChoice(array('model' => 'Player', 'add_empty' => true)),
      'medal'  => new sfWidgetFormDmFilterInput(),
    ));

    $this->setValidators(array(
      'player' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Player'), 'column' => 'id')),
      'medal'  => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));
    

    $this->widgetSchema->setNameFormat('player_medal_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'PlayerMedal';
  }

  public function getFields()
  {
    return array(
      'id'     => 'Number',
      'player' => 'ForeignKey',
      'medal'  => 'Number',
    );
  }
}
