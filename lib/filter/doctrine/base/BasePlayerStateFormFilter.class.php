<?php

/**
 * PlayerState filter form base class.
 *
 * @package    nzfs
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BasePlayerStateFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'player'         => new sfWidgetFormDoctrineChoice(array('model' => 'Player', 'add_empty' => true)),
      'health'         => new sfWidgetFormDmFilterInput(),
      'current_health' => new sfWidgetFormDmFilterInput(),
      'attack'         => new sfWidgetFormDmFilterInput(),
      'defense'        => new sfWidgetFormDmFilterInput(),
      'xp'             => new sfWidgetFormDmFilterInput(),
      'level'          => new sfWidgetFormDoctrineChoice(array('model' => 'Level', 'add_empty' => true)),
      'energy'         => new sfWidgetFormDmFilterInput(),
      'maximum_energy' => new sfWidgetFormDmFilterInput(),
      'money'          => new sfWidgetFormDmFilterInput(),
      'super_money'    => new sfWidgetFormDmFilterInput(),
      'rating'         => new sfWidgetFormDmFilterInput(),
      'created_at'     => new sfWidgetFormChoice(array('choices' => array(
        ''      => '',
        'today' => $this->getI18n()->__('Today'),
        'week'  => $this->getI18n()->__('Past %number% days', array('%number%' => 7)),
        'month' => $this->getI18n()->__('This month'),
        'year'  => $this->getI18n()->__('This year')
      ))),
      'updated_at'     => new sfWidgetFormChoice(array('choices' => array(
        ''      => '',
        'today' => $this->getI18n()->__('Today'),
        'week'  => $this->getI18n()->__('Past %number% days', array('%number%' => 7)),
        'month' => $this->getI18n()->__('This month'),
        'year'  => $this->getI18n()->__('This year')
      ))),
    ));

    $this->setValidators(array(
      'player'         => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Player'), 'column' => 'id')),
      'health'         => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'current_health' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'attack'         => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'defense'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'xp'             => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'level'          => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Leveling'), 'column' => 'id')),
      'energy'         => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'maximum_energy' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'money'          => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'super_money'    => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'rating'         => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'created_at'     => new sfValidatorChoice(array('required' => false, 'choices' => array_keys($this->widgetSchema['created_at']->getOption('choices')))),
      'updated_at'     => new sfValidatorChoice(array('required' => false, 'choices' => array_keys($this->widgetSchema['updated_at']->getOption('choices')))),
    ));
    

    $this->widgetSchema->setNameFormat('player_state_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'PlayerState';
  }

  public function getFields()
  {
    return array(
      'id'             => 'Number',
      'player'         => 'ForeignKey',
      'health'         => 'Number',
      'current_health' => 'Number',
      'attack'         => 'Number',
      'defense'        => 'Number',
      'xp'             => 'Number',
      'level'          => 'ForeignKey',
      'energy'         => 'Number',
      'maximum_energy' => 'Number',
      'money'          => 'Number',
      'super_money'    => 'Number',
      'rating'         => 'Number',
      'created_at'     => 'Date',
      'updated_at'     => 'Date',
    );
  }
}
