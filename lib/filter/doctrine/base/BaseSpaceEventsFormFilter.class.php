<?php

/**
 * SpaceEvents filter form base class.
 *
 * @package    nzfs
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseSpaceEventsFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'space_level' => new sfWidgetFormDoctrineChoice(array('model' => 'SpaceLevel', 'add_empty' => true)),
      'chance'      => new sfWidgetFormDmFilterInput(),
      'object'      => new sfWidgetFormDoctrineChoice(array('model' => 'SpaceObject', 'add_empty' => true)),
    ));

    $this->setValidators(array(
      'space_level' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('SpaceLevel'), 'column' => 'id')),
      'chance'      => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'object'      => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Object'), 'column' => 'id')),
    ));
    

    $this->widgetSchema->setNameFormat('space_events_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'SpaceEvents';
  }

  public function getFields()
  {
    return array(
      'id'          => 'Number',
      'space_level' => 'ForeignKey',
      'chance'      => 'Number',
      'object'      => 'ForeignKey',
    );
  }
}
