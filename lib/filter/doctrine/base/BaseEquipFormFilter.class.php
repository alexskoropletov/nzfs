<?php

/**
 * Equip filter form base class.
 *
 * @package    nzfs
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseEquipFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'name'        => new sfWidgetFormDmFilterInput(),
      'description' => new sfWidgetFormDmFilterInput(),
      'level'       => new sfWidgetFormDmFilterInput(),
      'price'       => new sfWidgetFormDmFilterInput(),
      'price_type'  => new sfWidgetFormChoice(array('choices' => array('' => '', 'money' => 'money', 'super_money' => 'super_money'))),
      'type'        => new sfWidgetFormChoice(array('choices' => array('' => '', 'weapon' => 'weapon', 'health' => 'health', 'defense' => 'defense'))),
      'modifier'    => new sfWidgetFormDmFilterInput(),
      'is_saleable' => new sfWidgetFormChoice(array('choices' => array('' => $this->getI18n()->__('yes or no', array(), 'dm'), 1 => $this->getI18n()->__('yes', array(), 'dm'), 0 => $this->getI18n()->__('no', array(), 'dm')))),
      'image'       => new sfWidgetFormDoctrineChoice(array('model' => 'DmMedia', 'add_empty' => true)),
      'image_small' => new sfWidgetFormDoctrineChoice(array('model' => 'DmMedia', 'add_empty' => true)),
    ));

    $this->setValidators(array(
      'name'        => new sfValidatorPass(array('required' => false)),
      'description' => new sfValidatorPass(array('required' => false)),
      'level'       => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'price'       => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'price_type'  => new sfValidatorChoice(array('required' => false, 'choices' => array('money' => 'money', 'super_money' => 'super_money'))),
      'type'        => new sfValidatorChoice(array('required' => false, 'choices' => array('weapon' => 'weapon', 'health' => 'health', 'defense' => 'defense'))),
      'modifier'    => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'is_saleable' => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'image'       => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Image'), 'column' => 'id')),
      'image_small' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('ImageSmall'), 'column' => 'id')),
    ));
    

    $this->widgetSchema->setNameFormat('equip_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Equip';
  }

  public function getFields()
  {
    return array(
      'id'          => 'Number',
      'name'        => 'Text',
      'description' => 'Text',
      'level'       => 'Number',
      'price'       => 'Number',
      'price_type'  => 'Enum',
      'type'        => 'Enum',
      'modifier'    => 'Number',
      'is_saleable' => 'Boolean',
      'image'       => 'ForeignKey',
      'image_small' => 'ForeignKey',
    );
  }
}
