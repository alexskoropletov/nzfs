<?php

/**
 * Medal filter form base class.
 *
 * @package    nzfs
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseMedalFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'name'                 => new sfWidgetFormDmFilterInput(),
      'description'          => new sfWidgetFormDmFilterInput(),
      'description_bonus'    => new sfWidgetFormDmFilterInput(),
      'image'                => new sfWidgetFormDoctrineChoice(array('model' => 'DmMedia', 'add_empty' => true)),
      'event'                => new sfWidgetFormDoctrineChoice(array('model' => 'SpaceEvents', 'add_empty' => true)),
      'result'               => new sfWidgetFormChoice(array('choices' => array('' => $this->getI18n()->__('yes or no', array(), 'dm'), 1 => $this->getI18n()->__('yes', array(), 'dm'), 0 => $this->getI18n()->__('no', array(), 'dm')))),
      'count'                => new sfWidgetFormDmFilterInput(),
      'type'                 => new sfWidgetFormChoice(array('choices' => array('' => '', 'permanent' => 'permanent', 'onetime' => 'onetime'))),
      'health_bonus'         => new sfWidgetFormDmFilterInput(),
      'attack_bonus'         => new sfWidgetFormDmFilterInput(),
      'defense_bonus'        => new sfWidgetFormDmFilterInput(),
      'xp_bonus'             => new sfWidgetFormDmFilterInput(),
      'energy_bonus'         => new sfWidgetFormDmFilterInput(),
      'maximum_energy_bonus' => new sfWidgetFormDmFilterInput(),
      'money_bonus'          => new sfWidgetFormDmFilterInput(),
      'rating_bonus'         => new sfWidgetFormDmFilterInput(),
    ));

    $this->setValidators(array(
      'name'                 => new sfValidatorPass(array('required' => false)),
      'description'          => new sfValidatorPass(array('required' => false)),
      'description_bonus'    => new sfValidatorPass(array('required' => false)),
      'image'                => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Image'), 'column' => 'id')),
      'event'                => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('SpaceEvent'), 'column' => 'id')),
      'result'               => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'count'                => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'type'                 => new sfValidatorChoice(array('required' => false, 'choices' => array('permanent' => 'permanent', 'onetime' => 'onetime'))),
      'health_bonus'         => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'attack_bonus'         => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'defense_bonus'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'xp_bonus'             => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'energy_bonus'         => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'maximum_energy_bonus' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'money_bonus'          => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'rating_bonus'         => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));
    

    $this->widgetSchema->setNameFormat('medal_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Medal';
  }

  public function getFields()
  {
    return array(
      'id'                   => 'Number',
      'name'                 => 'Text',
      'description'          => 'Text',
      'description_bonus'    => 'Text',
      'image'                => 'ForeignKey',
      'event'                => 'ForeignKey',
      'result'               => 'Boolean',
      'count'                => 'Number',
      'type'                 => 'Enum',
      'health_bonus'         => 'Number',
      'attack_bonus'         => 'Number',
      'defense_bonus'        => 'Number',
      'xp_bonus'             => 'Number',
      'energy_bonus'         => 'Number',
      'maximum_energy_bonus' => 'Number',
      'money_bonus'          => 'Number',
      'rating_bonus'         => 'Number',
    );
  }
}
