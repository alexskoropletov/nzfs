<?php

/**
 * PlayerStatistics filter form base class.
 *
 * @package    nzfs
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BasePlayerStatisticsFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'player' => new sfWidgetFormDoctrineChoice(array('model' => 'Player', 'add_empty' => true)),
      'event'  => new sfWidgetFormDoctrineChoice(array('model' => 'SpaceEvents', 'add_empty' => true)),
      'result' => new sfWidgetFormChoice(array('choices' => array('' => $this->getI18n()->__('yes or no', array(), 'dm'), 1 => $this->getI18n()->__('yes', array(), 'dm'), 0 => $this->getI18n()->__('no', array(), 'dm')))),
      'count'  => new sfWidgetFormDmFilterInput(),
    ));

    $this->setValidators(array(
      'player' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Player'), 'column' => 'id')),
      'event'  => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('SpaceEvent'), 'column' => 'id')),
      'result' => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'count'  => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));
    

    $this->widgetSchema->setNameFormat('player_statistics_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'PlayerStatistics';
  }

  public function getFields()
  {
    return array(
      'id'     => 'Number',
      'player' => 'ForeignKey',
      'event'  => 'ForeignKey',
      'result' => 'Boolean',
      'count'  => 'Number',
    );
  }
}
