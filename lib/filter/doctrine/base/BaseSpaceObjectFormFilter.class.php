<?php

/**
 * SpaceObject filter form base class.
 *
 * @package    nzfs
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseSpaceObjectFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'name'        => new sfWidgetFormDmFilterInput(),
      'description' => new sfWidgetFormDmFilterInput(),
      'type'        => new sfWidgetFormChoice(array('choices' => array('' => '', 'enemy' => 'enemy', 'enemy_boss' => 'enemy_boss', 'excavation' => 'excavation', 'crash' => 'crash'))),
      'attack'      => new sfWidgetFormDmFilterInput(),
      'health'      => new sfWidgetFormDmFilterInput(),
      'defense'     => new sfWidgetFormDmFilterInput(),
      'image'       => new sfWidgetFormDoctrineChoice(array('model' => 'DmMedia', 'add_empty' => true)),
      'experience'  => new sfWidgetFormDmFilterInput(),
      'money'       => new sfWidgetFormDmFilterInput(),
      'energy'      => new sfWidgetFormDmFilterInput(),
    ));

    $this->setValidators(array(
      'name'        => new sfValidatorPass(array('required' => false)),
      'description' => new sfValidatorPass(array('required' => false)),
      'type'        => new sfValidatorChoice(array('required' => false, 'choices' => array('enemy' => 'enemy', 'enemy_boss' => 'enemy_boss', 'excavation' => 'excavation', 'crash' => 'crash'))),
      'attack'      => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'health'      => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'defense'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'image'       => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Image'), 'column' => 'id')),
      'experience'  => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'money'       => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'energy'      => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));
    

    $this->widgetSchema->setNameFormat('space_object_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'SpaceObject';
  }

  public function getFields()
  {
    return array(
      'id'          => 'Number',
      'name'        => 'Text',
      'description' => 'Text',
      'type'        => 'Enum',
      'attack'      => 'Number',
      'health'      => 'Number',
      'defense'     => 'Number',
      'image'       => 'ForeignKey',
      'experience'  => 'Number',
      'money'       => 'Number',
      'energy'      => 'Number',
    );
  }
}
