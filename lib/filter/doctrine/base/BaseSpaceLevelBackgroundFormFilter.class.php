<?php

/**
 * SpaceLevelBackground filter form base class.
 *
 * @package    nzfs
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseSpaceLevelBackgroundFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'space_level' => new sfWidgetFormDoctrineChoice(array('model' => 'SpaceLevel', 'add_empty' => true)),
      'image'       => new sfWidgetFormDoctrineChoice(array('model' => 'DmMedia', 'add_empty' => true)),
    ));

    $this->setValidators(array(
      'space_level' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('SpaceLevel'), 'column' => 'id')),
      'image'       => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Image'), 'column' => 'id')),
    ));
    

    $this->widgetSchema->setNameFormat('space_level_background_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'SpaceLevelBackground';
  }

  public function getFields()
  {
    return array(
      'id'          => 'Number',
      'space_level' => 'ForeignKey',
      'image'       => 'ForeignKey',
    );
  }
}
