<?php

/**
 * Ship filter form base class.
 *
 * @package    nzfs
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseShipFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'name'        => new sfWidgetFormDmFilterInput(),
      'description' => new sfWidgetFormDmFilterInput(),
      'price'       => new sfWidgetFormDmFilterInput(),
      'price_type'  => new sfWidgetFormChoice(array('choices' => array('' => '', 'money' => 'money', 'super_money' => 'super_money'))),
      'attack'      => new sfWidgetFormDmFilterInput(),
      'health'      => new sfWidgetFormDmFilterInput(),
      'defense'     => new sfWidgetFormDmFilterInput(),
      'image'       => new sfWidgetFormDoctrineChoice(array('model' => 'DmMedia', 'add_empty' => true)),
      'image_small' => new sfWidgetFormDoctrineChoice(array('model' => 'DmMedia', 'add_empty' => true)),
    ));

    $this->setValidators(array(
      'name'        => new sfValidatorPass(array('required' => false)),
      'description' => new sfValidatorPass(array('required' => false)),
      'price'       => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'price_type'  => new sfValidatorChoice(array('required' => false, 'choices' => array('money' => 'money', 'super_money' => 'super_money'))),
      'attack'      => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'health'      => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'defense'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'image'       => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Image'), 'column' => 'id')),
      'image_small' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('ImageSmall'), 'column' => 'id')),
    ));
    

    $this->widgetSchema->setNameFormat('ship_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Ship';
  }

  public function getFields()
  {
    return array(
      'id'          => 'Number',
      'name'        => 'Text',
      'description' => 'Text',
      'price'       => 'Number',
      'price_type'  => 'Enum',
      'attack'      => 'Number',
      'health'      => 'Number',
      'defense'     => 'Number',
      'image'       => 'ForeignKey',
      'image_small' => 'ForeignKey',
    );
  }
}
