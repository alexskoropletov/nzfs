<?php
/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class Version13 extends Doctrine_Migration_Base
{
    public function up()
    {
        $this->addColumn('player', 'vk_id', 'integer', '8', array(
             ));
    }

    public function down()
    {
        $this->removeColumn('player', 'vk_id');
    }
}